/*
void _add_reg16_reg16(word *reg1, word *reg2, z80 *cpu);
void _adc_a_reg8(z80 *cpu, uint8_t *reg);
void _adc_a_mem(z80 *cpu, uint8_t *memory);
*/

#include "../config.h"

#include <check.h>

#if HAVE_ERR
# include <err.h>
#endif

#include <stdlib.h>

#include "check_add.h"
#include "../src/add.h"
#include "../src/utils.h"

static struct z80 *c;
static uint8_t n;

static void
setup(void)
{
	if ((c = calloc(sizeof(struct z80), 1)) == NULL)
		err(1, NULL);

	n = 0;
}

static void
teardown(void)
{
	free(c);
}

static void
inc8_common_assertions(uint8_t n, uint8_t x, struct z80 *c)
{
	ck_assert_uint_eq(n, x);
	ck_assert(!IS_SET(c->af.B.l, CARRY));
	ck_assert(!IS_SET(c->af.B.l, SUBTRACT));
}

static void
add_common_assertions(struct z80 *c, uint8_t x)
{
	ck_assert_uint_eq(c->af.B.h, x);
	ck_assert(!IS_SET(c->af.B.l, SUBTRACT));
}

START_TEST(test_inc8_increment)
{
	n = 1;

	_inc8(&n, c);

	inc8_common_assertions(n, 2, c);
	ck_assert(!IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(!IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(!IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_inc8_overflow)
{
	n = 255;

	_inc8(&n, c);

	inc8_common_assertions(n, 0, c);
	ck_assert(IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_inc8_halfcarry)
{
	n = 15;

	_inc8(&n, c);

	inc8_common_assertions(n, 16, c);
	ck_assert(!IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(!IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_inc8_sign)
{
	n = 127;

	_inc8(&n, c);

	inc8_common_assertions(n, 128, c);
	ck_assert(IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(!IS_SET(c->af.B.l, ZERO));
	ck_assert(IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_add_a_mem)
{
	_add_a_mem(c, &n);

	add_common_assertions(c, 0);
	ck_assert(!IS_SET(c->af.B.l, CARRY));
	ck_assert(!IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(!IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_add_a_mem_carry)
{
	c->af.B.h = 128;
	n = 128;

	_add_a_mem(c, &n);

	add_common_assertions(c, 0);
	ck_assert(IS_SET(c->af.B.l, CARRY));
	ck_assert(IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(!IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_add_a_mem_halfcarry)
{
	c->af.B.h = 8;
	n = 8;

	_add_a_mem(c, &n);

	add_common_assertions(c, 16);
	ck_assert(!IS_SET(c->af.B.l, CARRY));
	ck_assert(!IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(!IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_add_a_mem_sign)
{
	c->af.B.h = (uint8_t)(-24);
	n = (uint8_t)(-42);

	_add_a_mem(c, &n);

	add_common_assertions(c, (uint8_t)(-66));
	ck_assert(IS_SET(c->af.B.l, CARRY));
	ck_assert(IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(!IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(!IS_SET(c->af.B.l, ZERO));
	ck_assert(IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_add_a_reg8)
{
	_add_a_reg8(c, &n);

	add_common_assertions(c, 0);
	ck_assert(!IS_SET(c->af.B.l, CARRY));
	ck_assert(!IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(!IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_add_a_reg8_carry)
{
	c->af.B.h = 128;
	n = 128;

	_add_a_reg8(c, &n);

	add_common_assertions(c, 0);
	ck_assert(IS_SET(c->af.B.l, CARRY));
	ck_assert(IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(!IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_add_a_reg8_halfcarry)
{
	c->af.B.h = 8;
	n = 8;

	_add_a_reg8(c, &n);

	add_common_assertions(c, 16);
	ck_assert(!IS_SET(c->af.B.l, CARRY));
	ck_assert(!IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(!IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_add_a_reg8_sign)
{
	c->af.B.h = (uint8_t)(-24);
	n = (uint8_t)(-42);

	_add_a_reg8(c, &n);

	add_common_assertions(c, (uint8_t)(-66));
	ck_assert(IS_SET(c->af.B.l, CARRY));
	ck_assert(IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(!IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(!IS_SET(c->af.B.l, ZERO));
	ck_assert(IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_adc_a_reg8)
{
	SET(c->af.B.l, CARRY);
	_adc_a_reg8(c, &n);

	add_common_assertions(c, 1);
	ck_assert(!IS_SET(c->af.B.l, CARRY));
	ck_assert(!IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(!IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(!IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_adc_a_reg8_no_c)
{
	_adc_a_reg8(c, &n);

	add_common_assertions(c, 0);
	ck_assert(!IS_SET(c->af.B.l, CARRY));
	ck_assert(!IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(!IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_adc_a_reg8_carry_with_c)
{
	SET(c->af.B.l, CARRY);
	c->af.B.h = 128;
	n = 128;

	_adc_a_reg8(c, &n);

	add_common_assertions(c, 1);
	ck_assert(IS_SET(c->af.B.l, CARRY));
	ck_assert(IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(!IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(!IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

START_TEST(test_adc_a_reg8_carry_no_c)
{
	c->af.B.h = 128;
	n = 128;

	_adc_a_reg8(c, &n);

	add_common_assertions(c, 0);
	ck_assert(IS_SET(c->af.B.l, CARRY));
	ck_assert(IS_SET(c->af.B.l, PARITY_OVERFLOW));
	ck_assert(!IS_SET(c->af.B.l, HALF_CARRY));
	ck_assert(IS_SET(c->af.B.l, ZERO));
	ck_assert(!IS_SET(c->af.B.l, SIGN));
}
END_TEST

Suite *
add_suite(void)
{
	Suite *s;
	TCase *tc_adc_a_reg8;
	TCase *tc_add_a_mem;
	TCase *tc_add_a_reg8;
	TCase *tc_inc8;

	s = suite_create("Addition Helpers");

	tc_inc8 = tcase_create("8-Bit Increment Function");
	tc_add_a_mem = tcase_create("add a,op8 (mem) Helper Function");
	tc_add_a_reg8 = tcase_create("add a,op8 (reg) Helper Function");
	tc_adc_a_reg8 = tcase_create("adc a,op8 (reg) Helper Function");

	tcase_add_checked_fixture(tc_inc8, setup, teardown);
	tcase_add_test(tc_inc8, test_inc8_increment);
	tcase_add_test(tc_inc8, test_inc8_overflow);
	tcase_add_test(tc_inc8, test_inc8_halfcarry);
	tcase_add_test(tc_inc8, test_inc8_sign);

	tcase_add_checked_fixture(tc_add_a_mem, setup, teardown);
	tcase_add_test(tc_add_a_mem, test_add_a_mem);
	tcase_add_test(tc_add_a_mem, test_add_a_mem_carry);
	tcase_add_test(tc_add_a_mem, test_add_a_mem_halfcarry);
	tcase_add_test(tc_add_a_mem, test_add_a_mem_sign);

	tcase_add_checked_fixture(tc_add_a_reg8, setup, teardown);
	tcase_add_test(tc_add_a_reg8, test_add_a_reg8);
	tcase_add_test(tc_add_a_reg8, test_add_a_reg8_carry);
	tcase_add_test(tc_add_a_reg8, test_add_a_reg8_halfcarry);
	tcase_add_test(tc_add_a_reg8, test_add_a_reg8_sign);

	tcase_add_checked_fixture(tc_adc_a_reg8, setup, teardown);
	tcase_add_test(tc_adc_a_reg8, test_adc_a_reg8);
	tcase_add_test(tc_adc_a_reg8, test_adc_a_reg8_no_c);
	tcase_add_test(tc_adc_a_reg8, test_adc_a_reg8_carry_with_c);
	tcase_add_test(tc_adc_a_reg8, test_adc_a_reg8_carry_no_c);

	suite_add_tcase(s, tc_inc8);
	suite_add_tcase(s, tc_add_a_mem);
	suite_add_tcase(s, tc_add_a_reg8);
	suite_add_tcase(s, tc_adc_a_reg8);

	return s;
}
