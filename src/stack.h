/**
 * \file stack.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_STACK__
#define __PZ80EMU_STACK__

#include <stdbool.h>
#include <stdint.h>

#include "z80.h"

__BEGIN_DECLS
void _ret_flag(struct z80 *, uint8_t *, int, bool);
void _ret(struct z80 *, uint8_t *);
void _pop(word *, struct z80 *, uint8_t *);
void _call_flag_nn(struct z80 *, uint8_t *, int, bool);
void _call_nn(struct z80 *, uint8_t *);
void _push(word *, struct z80 *, uint8_t *);
void _rst(struct z80 *, uint8_t *, uint16_t);
__END_DECLS

#endif
