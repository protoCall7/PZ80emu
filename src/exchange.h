/**
 * \file exchange.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_EXCHANGE__
#define __PZ80EMU_EXCHANGE__

#include <stdint.h>

#include "z80.h"

__BEGIN_DECLS
void _ex_reg16(word *, word *);
void _exx(struct z80 *);
void _ex_sp_reg16(word *, struct z80 *, uint8_t *);
__END_DECLS

#endif
