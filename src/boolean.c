/**
 * \file boolean.c
 * \author Peter H. Ezetta
 */

#include "boolean.h"
#include "utils.h"

/**
 * perform boolean AND operation
 * \param reg register to operate on
 * \param cpu z80 cpu object
 */
void
_and(uint8_t *reg, struct z80 *cpu)
{
	RESET(cpu->af.B.l, CARRY);
	RESET(cpu->af.B.l, SUBTRACT);
	SET(cpu->af.B.l, HALF_CARRY);

	cpu->af.B.h &= *reg;
}

/**
 * perform boolean XOR operation
 * \param reg register to operate on
 * \param cpu z80 cpu object
 */
void
_xor(uint8_t *reg, struct z80 *cpu)
{
	RESET(cpu->af.B.l, CARRY);
	RESET(cpu->af.B.l, SUBTRACT);
	RESET(cpu->af.B.l, HALF_CARRY);

	cpu->af.B.h ^= *reg;
}

/**
 * perform boolean OR operation
 * \param reg register to operate on
 * \param cpu z80 cpu object
 */
void
_or(uint8_t *reg, struct z80 *cpu)
{
	RESET(cpu->af.B.l, CARRY);
	RESET(cpu->af.B.l, SUBTRACT);
	RESET(cpu->af.B.l, HALF_CARRY);

	cpu->af.B.h |= *reg;
}
