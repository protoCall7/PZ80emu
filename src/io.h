/**
 * \file io.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_IO__
#define __PZ80EMU_IO__

#include <stdint.h>

#include "z80.h"

__BEGIN_DECLS
void _out_mem_reg8(uint8_t, struct z80 *, uint8_t *);
void _out_c_reg8(uint8_t, struct z80 *, uint8_t *);
void _in_reg8_mem(uint8_t *, struct z80 *, uint8_t *);
void _otir(struct z80 *, uint8_t *);
void __clear_input_buffer(void);
__END_DECLS

#endif
