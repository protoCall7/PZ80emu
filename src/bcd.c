/**
 * \file bcd.c
 */

#include "../config.h"

#include "bcd.h"
#include "utils.h"

/**
 * Decimal Adjust A
 * \param cpu z80 cpu object
 */
void
_daa(struct z80 *cpu)
{
	uint8_t number = cpu->af.B.h;
	uint8_t low = number % 16;
	uint8_t high = 0;

	number /= 16;
	high = number % 16;

	if (!IS_SET(cpu->af.B.l, SUBTRACT)) {
		if (!IS_SET(cpu->af.B.l, CARRY)) {
			if ((high <= 0x9)) {
				if (!IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low <= 0x9)) {
						cpu->af.B.h += 0x00;
						RESET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
			if ((high <= 0x8)) {
				if (!IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low >= 0xA) && (low <= 0xF)) {
						cpu->af.B.h += 0x06;
						RESET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
			if ((high <= 0x9)) {
				if (IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low <= 0x3)) {
						cpu->af.B.h += 0x06;
						RESET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
			if ((high >= 0xA) && (high <= 0xF)) {
				if (!IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low <= 0x9)) {
						cpu->af.B.h += 0x60;
						SET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
			if ((high >= 0x9) && (high <= 0xF)) {
				if (!IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low >= 0xA) && (low <= 0xF)) {
						cpu->af.B.h += 0x66;
						SET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
			if ((high >= 0xA) && (high <= 0xF)) {
				if (IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low <= 0x3)) {
						cpu->af.B.h += 0x66;
						SET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
		} else {
			if ((high <= 0x2)) {
				if (!IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low <= 0x9)) {
						cpu->af.B.h += 0x60;
						SET(cpu->af.B.l, CARRY);
						return;
					}
					if ((low >= 0xA) && (low <= 0xF)) {
						cpu->af.B.h += 0x66;
						SET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
			if ((high <= 0x3)) {
				if (IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low <= 0x3)) {
						cpu->af.B.h += 0x66;
						SET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
		}
	} else {
		if (!IS_SET(cpu->af.B.l, CARRY)) {
			if ((high <= 0x9)) {
				if (!IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low <= 0x9)) {
						cpu->af.B.h += 0x00;
						RESET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
			if ((high <= 0x8)) {
				if (IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low >= 0x6) && (low <= 0xF)) {
						cpu->af.B.h += 0xFA;
						RESET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
		} else {
			if ((high >= 0x7) && (high <= 0xF)) {
				if (!IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low <= 0x9)) {
						cpu->af.B.h += 0xA0;
						SET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
			if ((high >= 0x6) && (high <= 0xF)) {
				if (IS_SET(cpu->af.B.l, HALF_CARRY)) {
					if ((low >= 0x6) && (low <= 0xF)) {
						cpu->af.B.h += 0x9A;
						SET(cpu->af.B.l, CARRY);
						return;
					}
				}
			}
		}
	}
}
