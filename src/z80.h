/** 
 * \file z80.h
 * \brief z80 CPU data structure and functions
 * \author Peter H. Ezetta
 */

#ifndef __PZ80emu__z80__
#define __PZ80emu__z80__

#include <stdint.h>

#include "bus.h"

/** \brief initial value of the PC register */
#define INIT_PC 0x0000

/** \brief number of cycles to run before triggering interrupt */
#define INTERRUPT_PERIOD 10240

/** \brief type to deal with endianness and access of high/low bits */
typedef union {
	uint16_t W; /**< \brief 16-bit pair */

	/** 
	 * \brief Combination of High and Low bytes in correct order for
	 * endianness.
	 */
	struct {
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
		/* ...in low-endian architecture */
		uint8_t l; /**< \brief low order byte */
		uint8_t h; /**< \brief high order byte */
#else
		/* ...in high-endian architecture */
		uint8_t h; /**< \brief high order byte */
		uint8_t l; /**< \brief low order byte */
#endif
	} B;
} word;

/**
 * \brief enum to contain all possible interrupt modes
 */
typedef enum {
	IM0,			/**< Interrupt Mode 0 */
	IM1,			/**< Interrupt Mode 1 */
	IM2			/**< Interrupt Mode 2 */
} interrupt_mode;

/** Collection of registers comprising a Z80 CPU */
struct z80 {
	int		counter;	/**< interrupt counter */
	word		pc;		/**< program counter */
	word		af;		/**< AF register pair */
	word		bc;		/**< BC register pair */
	word		de;		/**< DE register pair */
	word		hl;		/**< HL register pair */
	word		ix;		/**< IX register */
	word		iy;		/**< IY register */
	word		ir;		/**< IR register */
	word		_af;		/**< AF' register pair */
	word		_bc;		/**< BC' register pair */
	word		_de;		/**< DE' register pair */
	word		_hl;		/**< HL' register pair */
	word		sp;		/**< Stack Pointer */
	struct bus	*bus; 		/**< Address/Data Bus */
	interrupt_mode	im; 		/**< Interrupt Mode */
	bool		iff1; 		/**< Interrupt FlipFlop 1 */
	bool		iff2; 		/**< Interrupt FlipFlip 2 */
};

#define CARRY 0			/**< \brief bit position of CARRY (C) flag */
#define SUBTRACT 1		/**< \brief bit position of SUBTRACT (N) flag */
#define PARITY_OVERFLOW 2	/**< \brief bit position of OVERFLOW (P/V) flag */
#define F3 3			/**< \brief bit position of F3 flag */
#define HALF_CARRY 4		/**< \brief bit position of HALF CARRY (H) flag */
#define F5 5			/**< \brief bit position of F5 flag */
#define ZERO 6			/**< \brief bit position of ZERO (Z) flag */
#define SIGN 7			/**< \brief bit position of SIGN (S) flag */

__BEGIN_DECLS
struct z80 *new_cpu(void);
void reset_cpu(struct z80 *);
int run(struct z80 *, uint8_t *, long, bool);
__END_DECLS

#endif /* defined(__PZ80emu__z80__) */
