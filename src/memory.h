/** 
 * \file memory.h
 * \brief Memory setup functions
 * \author Peter H. Ezetta
 */

#ifndef __PZ80emu__memory__
#define __PZ80emu__memory__

#include <stdint.h>

/** Size of memory for create_ram() to allocate */
#define MEMSIZE 65536

/**
 * z80 memory class
 * \brief z80 memory class
 */
struct memory {
	/** Pointer to memory_free method */
	void	(*memory_free)(void *self);

	/** pointer to block of memory for allocation */
	uint8_t	*memory;

	/** pointer to memory_load method */
	long	(*memory_load)(void *self, const char *filename);
};

__BEGIN_DECLS
struct memory * memory_new(void);
__END_DECLS

#endif /* defined(__PZ80emu__memory__) */
