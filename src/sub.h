/**
 * \file sub.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_SUB__
#define __PZ80EMU_SUB__

#include "z80.h"

__BEGIN_DECLS
void _dec8(uint8_t *, struct z80 *);
void _sub_a_reg8(struct z80 *, uint8_t *);
void _sub_a_mem(struct z80 *, uint8_t *);
void _sbc_a_reg8(struct z80 *, uint8_t *);
void _cp(uint8_t *, struct z80 *);
__END_DECLS

#endif
