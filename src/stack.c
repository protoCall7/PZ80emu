/**
 * \file stack.c
 * \author Peter H. Ezetta
 */

#include "stack.h"
#include "utils.h"

/**
 * \brief returns from a call based on state of CPU flag
 * \param cpu z80 CPU object
 * \param memory pointer to allocated memory block
 * \param flag CPU flag to check for condition
 * \param n state of CPU flag
 */
void
_ret_flag(struct z80 *cpu, uint8_t *memory, int flag, bool n)
{
	word address;

	if (n) {
		if (IS_SET(cpu->af.B.l, flag))
			return;
		else {
			address.B.l = memory[(cpu->sp.W + 1)];
			address.B.h = memory[(cpu->sp.W + 2)];

			cpu->sp.W += 2;

			cpu->pc.W = address.W;
		}
	} else {
		if (IS_SET(cpu->af.B.l, flag)) {
			address.B.l = memory[(cpu->sp.W + 1)];
			address.B.h = memory[(cpu->sp.W + 2)];

			cpu->sp.W += 2;

			cpu->pc.W = address.W;
		}
	}
}

/**
 * \brief return from a call
 * \param cpu z80 CPU object
 * \param memory pointer to allocated memory block
 */
void
_ret(struct z80 *cpu, uint8_t *memory)
{
	word address;

	address.B.l = memory[(cpu->sp.W + 1)];
	address.B.h = memory[(cpu->sp.W + 2)];

	cpu->sp.W += 2;

	cpu->pc.W = address.W;
}

/**
 * \brief pop a value off the stack into a register
 * \param reg register to pop value from stack into
 * \param cpu z80 CPU object
 * \param memory pointer to allocated memory block
 */
void
_pop(word *reg, struct z80 *cpu, uint8_t *memory)
{
	reg->B.l = memory[(cpu->sp.W + 1)];
	reg->B.h = memory[(cpu->sp.W + 2)];

	cpu->sp.W += 2;
}

/**
 * \brief make a function call based on status of CPU flags
 * \param cpu z80 CPU object
 * \param memory pointer to allocated memory block
 * \param flag CPU flag to check before calling function
 * \param n state of checked flag
 */
void
_call_flag_nn(struct z80 *cpu, uint8_t *memory, int flag, bool n)
{
	word nn;

	nn.B.l = memory[cpu->pc.W++];
	nn.B.h = memory[cpu->pc.W++];

	if (n) {
		if (IS_SET(cpu->af.B.l, flag))
			return;
		else {
			memory[--cpu->sp.W] = cpu->pc.B.h;
			memory[--cpu->sp.W] = cpu->pc.B.l;

			cpu->pc = nn;
		}
	} else {
		if (IS_SET(cpu->af.B.l, flag)) {
			memory[--cpu->sp.W] = cpu->pc.B.h;
			memory[--cpu->sp.W] = cpu->pc.B.l;

			cpu->pc = nn;
		}
	}
}

/**
 * \brief calls the function at memory address NN
 * \param cpu z80 CPU object
 * \param memory pointer to allocated memory block
 */
void
_call_nn(struct z80 *cpu, uint8_t *memory)
{
	word nn;

	nn.B.l = memory[cpu->pc.W++];
	nn.B.h = memory[cpu->pc.W++];

	cpu->sp.W -= 2;
	memory[(cpu->sp.W + 1)] = cpu->pc.B.l;
	memory[(cpu->sp.W + 2)] = cpu->pc.B.h;

	cpu->pc = nn;
}

/**
 * \brief pushes the value of a register to the stack
 * \param reg register to push
 * \param cpu z80 CPU object
 * \param memory pointer to allocated memory
 */
void
_push(word *reg, struct z80 *cpu, uint8_t *memory)
{
	cpu->sp.W -= 2;

	memory[(cpu->sp.W + 1)] = reg->B.l;
	memory[(cpu->sp.W + 2)] = reg->B.h;
}

/**
 * \brief saves PC to the stack, then sets a new PC
 * \param cpu z80 CPU object
 * \param memory pointer to allocated memory
 * \param pc new memory address to place in PC
 */
void
_rst(struct z80 *cpu, uint8_t *memory, uint16_t pc)
{
	cpu->sp.W -= 2;
	memory[(cpu->sp.W + 1)] = cpu->pc.B.l;
	memory[(cpu->sp.W + 2)] = cpu->pc.B.h;

	cpu->pc.W = pc;
}
