/**
 * \file interpreter.h
 */

#ifndef __PZ80EMU_INTERPRETER__
#define __PZ80EMU_INTERPRETER__

#include <stdint.h>

#include "z80.h"

__BEGIN_DECLS
void _instruction(struct z80 *, uint8_t *);
__END_DECLS

#endif
