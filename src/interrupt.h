/**
 * \file interrupt.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_INTERRUPT__
#define __PZ80EMU_INTERRUPT__

#include "z80.h"

__BEGIN_DECLS
void _di(struct z80 *);
void _ei(struct z80 *);
__END_DECLS

#endif
