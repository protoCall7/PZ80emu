/**
 * \file load.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_LOAD__
#define __PZ80EMU_LOAD__

#include <stdint.h>

#include "z80.h"

__BEGIN_DECLS
void _load_reg8_mem_idx_offset(uint8_t *, word *, uint8_t *, word *);
void _load_mem_idx_offset_reg8(uint8_t *, word *, uint8_t *, word *);
void _load_idx_offset_n(word *, uint8_t *, word *);
void _load_a_mem(struct z80 *, uint8_t *);
void _load_mem_a(struct z80 *, uint8_t *);
void _load_reg16_nn(word *, uint8_t *, word *);
void _load_mem_reg16(word *, uint8_t *, word *);
void _load_reg16_mem(word *, uint8_t *, word *);
__END_DECLS

#endif
