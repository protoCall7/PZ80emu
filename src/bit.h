#ifndef __PZ80EMU_BIT__
#define __PZ80EMU_BIT__

#include "z80.h"

__BEGIN_DECLS
void _bit(uint8_t, struct z80 *, uint8_t);
void _sla(uint8_t *, struct z80 *);
void _srl(uint8_t *, struct z80 *);
void _rl(uint8_t *, struct z80 *);
__END_DECLS

#endif
