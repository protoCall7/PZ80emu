/**
 * \file load.c
 * \author Peter H. Ezetta
 */

#include "load.h"
#include "memory.h"

/**
 * Loads a value into an 8-bit register from the memory location stored in an index register + an offset from memory.
 * \param reg register to load
 * \param index_register pointer to ix or iy index register
 * \param memory block of memory containing the value to load
 * \param pc pointer to program counter
 */
void 
_load_reg8_mem_idx_offset(uint8_t *reg, word *index_register, uint8_t *memory, word *pc)
{
	uint8_t index = memory[pc->W++];

	*reg = memory[(index + index_register->W)];
}

/**
 * Loads a value into a memory location stored in an index register + an offset from a register.
 * \param reg register to load from
 * \param index_register pointer to ix or iy index register
 * \param memory block of memory containing the value to load
 * \param pc pointer to program counter
 */
void
_load_mem_idx_offset_reg8(uint8_t *reg, word *index_register, uint8_t *memory, word *pc)
{
	uint8_t index = memory[pc->W++];

	memory[(index + index_register->W)] = *reg;
}

/**
 * \param index_register
 * \param memory
 * \param pc
 */
void
_load_idx_offset_n(word *index_register, uint8_t *memory, word *pc)
{
	uint8_t index;
	index = memory[pc->W++];

	memory[(index + index_register->W)] = memory[pc->W++];
}

/**
 * \brief loads user provided value from memory into register A
 * \param cpu z80 CPU object
 * \param memory pointer to allocated block of memory
 */
void
_load_a_mem(struct z80 *cpu, uint8_t *memory)
{
	word address;

	address.B.l = memory[cpu->pc.W++];
	address.B.h = memory[cpu->pc.W++];

	cpu->af.B.h = memory[address.W];
}

/**
 * \brief loads the value of A into a user provided memory location
 * \param cpu z80 CPU object
 * \param memory pointer to allocated block of memory
 */
void
_load_mem_a(struct z80 *cpu, uint8_t *memory)
{
	word address;

	address.B.l = memory[cpu->pc.W++];
	address.B.h = memory[cpu->pc.W++];

	memory[address.W] = cpu->af.B.h;
}

/**
 * Loads a user supplied value into a 16 bit register pair
 * \param reg register to load
 * \param memory block of memory to retrieve nn from
 * \param pc pointer to program counter
 */
void
_load_reg16_nn(word *reg, uint8_t *memory, word *pc)
{
	word nn;

	nn.B.l = memory[pc->W++];
	nn.B.h = memory[pc->W++];

	reg->W = nn.W;
}

/**
 * \brief loads a 16-bit register pair with a user-provided value from memory
 * \param reg register pair to load
 * \param memory pointer to allocated block of memory
 * \param pc pointer to program counter register
 * \todo refactor PC to use z80 CPU object instead
 */
void
_load_mem_reg16(word *reg, uint8_t *memory, word *pc)
{
	word address;

	address.B.l = memory[pc->W++];
	address.B.h = memory[pc->W++];

	memory[address.W++] = reg->B.l;
	memory[address.W] = reg->B.h;
}

/**
 * \brief loads a user-provided value from memory into a 16-bit register pair
 * \param reg 16-bit register pair
 * \param memory pointer to allocated block of memory
 * \param pc pointer to program counter
 * \todo refactor PC to use z80 CPU object instead
 */
void
_load_reg16_mem(word *reg, uint8_t *memory, word *pc)
{
	word address;

	address.B.l = memory[pc->W++];
	address.B.h = memory[pc->W++];

	reg->B.l = memory[address.W++];
	reg->B.h = memory[address.W];
}
