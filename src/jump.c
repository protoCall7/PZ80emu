/**
 * \file jump.c
 * \author Peter H. Ezetta
 */

#include "jump.h"
#include "utils.h"

/**
 * \brief decrements B, then jumps to the provided memory address if B is 0
 * \param cpu z80 CPU object
 * \param memory pointer to allocated memory
 */
void
_djnz_n(struct z80 *cpu, uint8_t *memory)
{
	int8_t offset;

	cpu->bc.B.h--;
	if (cpu->bc.B.h != 0) {
		offset = (int8_t) memory[cpu->pc.W++];
		cpu->pc.W += offset;
	}
}

/**
 * \brief adds n to PC if provided conditions are met
 * \param cpu z80 CPU object
 * \param memory pointer to allocated block of memory
 * \param flag CPU flag to operate on
 * \param n whether the flag must be set or unset
 *
 * _jr() will add the user provided value from memory to PC in the event that
 * the state of the provided flag matches the provided value of N.
 */
void
_jr(struct z80 *cpu, uint8_t *memory, int flag, bool n)
{
	int8_t offset;

	if (n) {
		if (IS_SET(cpu->af.B.l, flag)) {
			cpu->pc.W++;
			return;
		} else {
			offset = memory[cpu->pc.W++];
			cpu->pc.W += offset;
		}
	} else {
		if (IS_SET(cpu->af.B.l, flag)) {
			offset = memory[cpu->pc.W++];
			cpu->pc.W += offset;
		} else
			cpu->pc.W++;
	}
}

/**
 * \brief jumps to an arbitrary memory address
 * \param cpu z80 CPU object
 * \param memory pointer to allocated memory
 */
void
_jp_nn(struct z80 *cpu, uint8_t *memory)
{
	word nn;

	nn.B.l = memory[cpu->pc.W++];
	nn.B.h = memory[cpu->pc.W++];
	cpu->pc = nn;
}

/**
 * \brief jumps to N if provided conditions are met
 * \param cpu z80 CPU object
 * \param memory pointer to allocated block of memory
 * \param flag CPU flag to operate on
 * \param n whether the flag must be set or unset
 *
 * _jp_flag_nn() will jump to the user provided value from memory in the event
 * that the state of the provided flag matches the provided value of N.
 */
void
_jp_flag_nn(struct z80 *cpu, uint8_t *memory, int flag, bool n)
{
	word nn;

	if (n) {
		if (IS_SET(cpu->af.B.l, flag)) {
			cpu->pc.W += 2;
			return;
		} else {
			nn.B.l = memory[cpu->pc.W++];
			nn.B.h = memory[cpu->pc.W++];
			cpu->pc = nn;
		}
	} else {
		if (IS_SET(cpu->af.B.l, flag)) {
			nn.B.l = memory[cpu->pc.W++];
			nn.B.h = memory[cpu->pc.W++];
			cpu->pc = nn;
		} else
			cpu->pc.W += 2;
	}
}

/**
 * \brief jumps to the value stored in reg
 * \param reg register containing value to jump to
 * \param cpu z80 cpu object
 */
void
_jp_reg(word *reg, struct z80 *cpu)
{
	cpu->pc.W = reg->W;
}
