/**
 * \file boolean.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_BOOLEAN__
#define __PZ80EMU_BOOLEAN__

#include "z80.h"

__BEGIN_DECLS
void _and(uint8_t *, struct z80 *);
void _xor(uint8_t *, struct z80 *);
void _or(uint8_t *, struct z80 *);
__END_DECLS

#endif
