#include "bit.h"
#include "utils.h"
#include "z80.h"

static int
__calculate_parity(uint8_t value)
{
	int count = 0;
	while (value) {
		count += value & 1;  // Add the LSB to the count
		value = value >> 1;  // Shift right by one bit
	}
	return (count % 2 == 0); // Return 1 if even parity, 0 if odd
}

void
_bit(uint8_t reg, struct z80 *cpu, uint8_t offset)
{
	RESET(cpu->af.B.l, PARITY_OVERFLOW);
	SET(cpu->af.B.l, HALF_CARRY);

	if (IS_SET(reg, offset))
		RESET(cpu->af.B.l, ZERO);
	else
		SET(cpu->af.B.l, ZERO);
}

void
_sla(uint8_t *reg, struct z80 *cpu)
{
	/* shift MSB into C flag */
	if (IS_SET(*reg, 7))
		SET(cpu->af.B.l, CARRY);
	else
		RESET(cpu->af.B.l, CARRY);

	/* perform the shift */
	*reg = (*reg << 1);

	/* reset H and N flags */
	RESET(cpu->af.B.l, HALF_CARRY);
	RESET(cpu->af.B.l, SUBTRACT);

	/* handle S flag */
	if (*reg & 0x80)
		SET(cpu->af.B.l, SIGN);
	else
		RESET(cpu->af.B.l, SIGN);

	/* handle Z flag */
	if (*reg == 0)
		SET(cpu->af.B.l, ZERO);
	else
		RESET(cpu->af.B.l, ZERO);

	/* handle P/V flag */
	if ((*reg & 0x01) == 0)
		SET(cpu->af.B.l, PARITY_OVERFLOW);
	else
		RESET(cpu->af.B.l, PARITY_OVERFLOW);
}

void
_srl(uint8_t *reg, struct z80 *cpu)
{
	if (*reg & 0x01)
		SET(cpu->af.B.l, CARRY);
	else
		RESET(cpu->af.B.l, CARRY);

	*reg = (*reg >> 1);

	RESET(cpu->af.B.l, SIGN);
	RESET(cpu->af.B.l, HALF_CARRY);
	RESET(cpu->af.B.l, SUBTRACT);

	if (*reg == 0)
		SET(cpu->af.B.l, ZERO);
	else
		RESET(cpu->af.B.l, ZERO);

	if (__calculate_parity(*reg))
		SET(cpu->af.B.l, PARITY_OVERFLOW);
	else
		RESET(cpu->af.B.l, PARITY_OVERFLOW);
}

void
_rl(uint8_t *reg, struct z80 *cpu)
{
	uint8_t lsb = 0x00;

	/* if the C flag is set, set LSB to 1 */
	if (IS_SET(cpu->af.B.l, CARRY))
		lsb = 0x01;

	/* shift the MSB into the C flag */
	if (IS_SET(*reg, 7))
		SET(cpu->af.B.l, CARRY);
	else
		RESET(cpu->af.B.l, CARRY);

	/* rotate reg and set the LSB */
	*reg = (*reg << 1) | lsb;

	/* reset H and N flags */
	RESET(cpu->af.B.l, HALF_CARRY);
	RESET(cpu->af.B.l, SUBTRACT);

	/* handle S flag */
	if (*reg & 0x80)
		SET(cpu->af.B.l, SIGN);
	else
		RESET(cpu->af.B.l, SIGN);

	/* handle Z flag */
	if (*reg == 0)
		SET(cpu->af.B.l, ZERO);
	else
		RESET(cpu->af.B.l, ZERO);

	if (__calculate_parity(*reg))
		SET(cpu->af.B.l, PARITY_OVERFLOW);
	else
		RESET(cpu->af.B.l, PARITY_OVERFLOW);
}
