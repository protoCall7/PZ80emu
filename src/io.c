/**
 * \file io.c
 */

#include <stdio.h>

#include "io.h"
#include "utils.h"
#include "z80.h"

/**
 * \brief places the contents of an 8-bit register on the databus
 * \param reg register to output
 * \param cpu z80 CPU object
 * \param memory pointer to allocated memory
 */
void
_out_mem_reg8(uint8_t reg, struct z80 *cpu, uint8_t *memory)
{
	word port;

	port.B.l = memory[cpu->pc.W++];
	port.B.h = 0x00;
	cpu->bus->address = port.W;
	cpu->bus->data = reg;
}

void
_out_c_reg8(uint8_t reg, struct z80 *cpu, uint8_t *memory)
{
	word port;

	port.B.l = cpu->bc.B.l;
	port.B.h = 0x00;
	cpu->bus->address = port.W;
	cpu->bus->data = reg;
}

/**
 * \brief reads value of data bus into an 8-bit register
 * \param reg register to store input value to
 * \param cpu z80 CPU object
 * \param memory pointer to allocated memory
 */
void
_in_reg8_mem(uint8_t *reg, struct z80 *cpu, uint8_t *memory)
{
	word port;
	uint8_t data = 0;

	port.B.l = memory[cpu->pc.W++];
	port.B.h = 0x00;
	cpu->bus->address = port.W;

	printf("Enter data for bus address 0x%02hhX: ", port.W);
	scanf("%hhX", &data);
	__clear_input_buffer();

	cpu->af.B.h = data;
}

void
_otir(struct z80 *cpu, uint8_t *memory)
{
	word port;

	do {
		cpu->bc.B.h--;
		port.B.l = cpu->bc.B.l;
		port.B.h = 0x00;
		cpu->bus->address = port.W;
		cpu->bus->data = memory[cpu->hl.W++];
	} while (cpu->bc.B.h > 0);

	SET(cpu->af.B.l, CARRY);
	RESET(cpu->af.B.l, SUBTRACT);
}

void
__clear_input_buffer(void)
{
	int c;
	while ((c = getchar()) != '\n' && c != EOF) {
		// Loop until the end of the line or file.
	}
}
