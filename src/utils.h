/** 
 * \file utils.h
 * \brief Utility macros
 * \author Peter H. Ezetta
 * \date 2016-02-27
 */

#ifndef __PZ80emu__utils__
#define __PZ80emu__utils__

/** 
 * \brief Determines if a bit is set or not
 * \param val Bitfield to check
 * \param bit Position of bit in field
 * \return 0 == Checked Bit False, 1 == Checked Bit True.
 */
#define IS_SET(val, bit) (bool)((val) & (1 << (bit)))

/**
 * \brief Sets a bit
 * \param val Bitfield to check
 * \param bit Position of bit in field
 */
#define SET(val, bit) ((val) |= (1 << (bit)))

/**
 * \brief Resets a bit
 * \param val Bitfield to check
 * \param bit Position of bit in field
 */
#define RESET(val, bit) ((val) &= ~(1 << (bit)))

/**
 * \brief Toggles a bit
 * \param val Bitfield to check
 * \param bit Position of bit in field
 */
#define TOGGLE(val, bit) ((val) ^= (1 << (bit)))

#endif /* defined __PZ80emu__utils__ */
