/**
 * \file display.h
 * \author Peter H. Ezetta
 * \date 2015-05-03
 */

#ifndef __PZ80emu__display__
#define __PZ80emu__display__

#include <ncurses.h>
#include <stdint.h>
#include <stdio.h>

#include "z80.h"

__BEGIN_DECLS
void curses_display_mem(WINDOW *, uint8_t *);
void curses_display_registers(WINDOW *, struct z80 *);
WINDOW *create_newwin(int, int, int, int);
void create_newscreen(int, int);
void display_mem(uint8_t *);
void display_registers(struct z80 *);
void display_bus(struct bus *);
__END_DECLS

#endif /* defined(__PZ80emu__display__) */
