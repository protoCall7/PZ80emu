/** 
 * \file pz80emu.c
 * \brief Main application
 *
 * \mainpage
 * PZ80emu
 *
 * Created by Peter Ezetta on 5/2/15.
 * Copyright (c) 2015-2017 Peter Ezetta. All rights reserved.
 *
 * \todo Implement rest of instruction set
 * \todo Implement menu system
 * \todo Fix error handling and reporting
 */

#include "../config.h"

#ifdef HAVE_ERR
# include <err.h>
#endif

#include <ctype.h>
#include <limits.h>
#include <ncurses.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "display.h"
#include "memory.h"
#include "z80.h"

/**
 * \brief Display usage text and exit
 */
static void
usage(void)
{
	errx(1, "Usage: %s -f <filename> -r <runcycles>", getprogname());
}

/** 
 * \brief PZ80 Machine Emulator
 * \param argc number of arguments passed to application
 * \param argv pointer to argument array
 * \todo do something with return from run()
 * \return application exit status
 */
int
main(int argc, char *argv[])
{
	struct z80 *cpu;
	struct memory *mem;
	long runcycles = 0, filesize = 0;
	int c;
	const char *errstr;
	bool s_flag = false;
	
	extern char *optarg;
	extern int optind, optopt;

#if HAVE_PLEDGE
	if (pledge("stdio rpath", NULL) == -1)
		err(1, NULL);
#endif

	mem = memory_new();

	while ((c = getopt(argc, argv, "sr:f:")) != -1) {
		switch (c) {
		case 'r':
			runcycles = strtonum(optarg, 0, INT_MAX, &errstr);
			if (errstr != NULL) {
				warnx("number is %s: %s", errstr, optarg);
				usage();
			}
			break;

		case 'f':
			filesize = mem->memory_load(mem, optarg);
			break;

		case 's':
			s_flag = true;
			break;
		}
	}

	/* make sure we got the required options, display help text if not */
	if (runcycles <= 0) {
		if (!s_flag)
			usage();
	}

	if (filesize <= 0)
		usage();

	cpu = new_cpu();

	/* execute! */
	(void)run(cpu, mem->memory, runcycles, s_flag);

	/* display stuff */
	display_registers(cpu);
	display_bus(cpu->bus);
	display_mem(mem->memory);

	/* memory cleanup (leaks are bad, mmkay?) */
	free(cpu->bus);
	free(cpu);
	mem->memory_free(mem);

	return 0;
}
