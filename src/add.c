/**
 * \file add.c
 * \brief Helper functions for addition operations
 * \author Peter H. Ezetta
 * \date 2018-2019
 */

#include "../config.h"

#include "add.h"
#include "memory.h"
#include "utils.h"

static void
carry_handler(struct z80 *cpu)
{
	if (IS_SET(cpu->af.B.l, CARRY))
		cpu->af.B.h++;
}

/**
 * \brief sets the carry flag based on the provided 8-bit operands
 * \param a first operand
 * \param b second operand
 * \param cpu z80 cpu object
 */
static void
___add8_set_carry_flag(uint8_t a, uint8_t b, struct z80 *cpu)
{
	if ((a + b) > 255)
		SET(cpu->af.B.l, CARRY);
	else
		RESET(cpu->af.B.l, CARRY);
}

/**
 * \brief sets the zero flag based on the provided 8-bit operands
 * \param a first operand
 * \param b second operand
 * \param cpu z80 cpu object
 */
static void
___add8_set_zero_flag(uint8_t a, uint8_t b, struct z80 *cpu)
{
	if ((uint8_t)(a + b) == 0)
		SET(cpu->af.B.l, ZERO);
	else
		RESET(cpu->af.B.l, ZERO);
}

/**
 * \brief sets the overflow flag based on the provided 8-bit operands
 * \param a first operand
 * \param b second operand
 * \param cpu z80 cpu object
 *
 * For addition operations, the P/V flag is used for overflow. It is
 * set when the 2's Complement result of the operation will not fit
 * in the register.
 */
static void
___add8_set_overflow_flag(uint8_t a, uint8_t b, struct z80 *cpu)
{
	if (((a + b) > 127) || ((signed)(a + b) < -128))
		SET(cpu->af.B.l, PARITY_OVERFLOW);
	else
		RESET(cpu->af.B.l, PARITY_OVERFLOW);
}

/**
 * \brief sets the sign flag based on the provided 8-bit operands
 * \param a first operand
 * \param b second operand
 * \param cpu z80 cpu object
 *
 * The S flag is a copy of bit 7 of the result of the operation.
 */
static void
___add8_set_sign_flag(uint8_t a, uint8_t b, struct z80 *cpu)
{
	if (IS_SET(a + b, 7))
		SET(cpu->af.B.l, SIGN);
	else
		RESET(cpu->af.B.l, SIGN);
}

/**
 * \brief sets the half carry flag based on the provided 8-bit operands
 * \param a first operand
 * \param b second operand
 * \param cpu z80 cpu object
 *
 * The H flag is set when there is a carry from bit 3 to bit 4.
 */
static void
___add8_set_half_carry_flag(uint8_t a, uint8_t b, struct z80 *cpu)
{
	if ((((a & 0x0F) + (b & 0x0F)) & 0x10) == 0x10)
		SET(cpu->af.B.l, HALF_CARRY);
	else
		RESET(cpu->af.B.l, HALF_CARRY);
}

/**
 * \brief wrapper to set all flags based on the provided addition operands
 * \param a first operand
 * \param b second operand
 * \param cpu z80 cpu object
 * \note INC doesn't effect C. Do not use this with _inc8() et al.
 */
static void
__add8_set_flags(uint8_t a, uint8_t b, struct z80 *cpu)
{
	___add8_set_carry_flag(a, b, cpu);
	___add8_set_zero_flag(a, b, cpu);
	___add8_set_overflow_flag(a, b, cpu);
	___add8_set_sign_flag(a, b, cpu);
	___add8_set_half_carry_flag(a, b, cpu);

	RESET(cpu->af.B.l, SUBTRACT);
}

/**
 * \brief adds the contents of a user supplied register to A
 * \param cpu z80 cpu object
 * \param reg register to add to A
 */
void
_add_a_reg8(struct z80 *cpu, uint8_t *reg)
{
	__add8_set_flags(cpu->af.B.h, *reg, cpu);
	cpu->af.B.h += *reg;
}

/**
 * \brief adds the contents of a user supplied register and the carry flag to A
 * \param cpu z80 cpu object
 * \param reg register to add to A
 */
void
_adc_a_reg8(struct z80 *cpu, uint8_t *reg)
{
	carry_handler(cpu);
	_add_a_reg8(cpu, reg);
}

/**
 * \brief adds the contents of a user supplied register to A
 * \param cpu z80 cpu object
 * \param memory pointer to system memory
 */
void
_add_a_mem(struct z80 *cpu, uint8_t *memory)
{
	uint8_t value = memory[cpu->pc.W++];

	__add8_set_flags(cpu->af.B.h, value, cpu);
	cpu->af.B.h += value;
}

/**
 * \brief adds contents of A to the contents of a memory address with carry
 * \param cpu z80 cpu object
 * \param memory memory location to add to A
 */
void
_adc_a_mem(struct z80 *cpu, uint8_t *memory)
{
	carry_handler(cpu);
	_add_a_mem(cpu, memory);
}

/**
 * \brief adds two 16-bit registers together, storing the values in reg1
 * \param reg1 the first register for the add operation
 * \param reg2 the second register for the add operation
 * \param cpu z80 cpu object
 */
void
_add_reg16_reg16(word *reg1, word *reg2, struct z80 *cpu)
{
	/* reset N flag */
	RESET(cpu->af.B.l, SUBTRACT);

	/* check for carry and add */
	if ((reg1->W + reg2->W) > 0xFFFF) {
		reg1->W += reg2->W;
		SET(cpu->af.B.l, CARRY);
		reg1->W -= 65536;
	} else {
		reg1->W += reg2->W;
		RESET(cpu->af.B.l, CARRY);
	}
}

/**
 * \brief increments an 8 bit register
 * \param n pointer to register to increment
 * \param cpu z80 cpu object
 */
void
_inc8(uint8_t *n, struct z80 *cpu)
{
	RESET(cpu->af.B.l, SUBTRACT);

	___add8_set_zero_flag(*n, 1, cpu);
	___add8_set_overflow_flag(*n, 1, cpu);
	___add8_set_sign_flag(*n, 1, cpu);
	___add8_set_half_carry_flag(*n, 1, cpu);

	*n += 1;
}
