/** 
 * \file bus.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_BUS__
#define __PZ80EMU_BUS__

#include <stdbool.h>
#include <stdint.h>

/**
 * \brief z80 bus class
 */
struct bus {
	bool 		busreq;    /**< BUSREQ signal */
	bool 		interrupt; /**< INT signal */
	bool 		nmi;       /**< NMI signal */
	uint8_t		data;      /**< 8-bit data bus */
	uint16_t 	address;   /**< 16-bit address bus */
};

#endif
