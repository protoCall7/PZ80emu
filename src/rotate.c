/**
 * \file rotate.c
 * \author Peter H. Ezetta
 */

#include "rotate.h"
#include "utils.h"

/**
 * \brief 8-bit rotation to the left
 * \param n
 * \param cpu z80 CPU object
 * \bug not rotating into the C flag
 */
void 
_rlc(uint8_t n, struct z80 *cpu)
{
	if (IS_SET(n, 7))
		SET(cpu->af.B.l, CARRY);
	else
		RESET(cpu->af.B.l, CARRY);

	n = (n << 1) | (n >> (sizeof(n) * 8 - 1));
}

/**
 * \brief register A 8-bit right rotate with carry
 * \param cpu z80 CPU object
 */
void
_rrca(struct z80 *cpu)
{
	if(IS_SET(cpu->af.B.h, 0))
		SET(cpu->af.B.l, CARRY);
	else
		RESET(cpu->af.B.l, CARRY);

	cpu->af.B.h = (cpu->af.B.h >> (sizeof(cpu->af.B.h) * 8 - 1)) | (cpu->af.B.h >> 1);
}
