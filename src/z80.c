/** 
 * \file z80.c
 * \author Peter H. Ezetta
 * \date 2015-05-03
 */

#include "../config.h"

#ifdef HAVE_ERR
# include <err.h>
#endif

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "display.h"
#include "interpreter.h"
#include "z80.h"

/**
 * Fills out a new z80 CPU struct
 * \return A z80 struct
 */
struct z80 *
new_cpu(void)
{
	struct z80 *cpu;

	if ((cpu = calloc(1, sizeof(struct z80))) == NULL)
		err(1, NULL);

	if ((cpu->bus = calloc(1, sizeof(struct bus))) == NULL)
		err(1, NULL);

	cpu->bus->busreq = true;
	cpu->bus->nmi = true;
	cpu->bus->interrupt = true;

	return cpu;
}

/**
 * Triggers the reset state on the z80 CPU
 * \param cpu A z80 struct to reset.
 */
void
reset_cpu(struct z80 *cpu)
{
	cpu->iff1 = false;
	cpu->iff2 = false;
	cpu->pc.W = 0x0000;
	cpu->ir.W = 0x0000;
	cpu->im = IM0;
}

/**
 * Runs the cpu
 * \param cpu A z80 cpu struct to run.
 * \param memory An allocated block of memory to pass to the cpu.
 * \param runcycles The number of clock cycles to run the cpu.
 * \param s_flag Enable running in step mode
 * \return Count of cycles executed.
 */
int
run(struct z80 *cpu, uint8_t *memory, long runcycles, bool s_flag)
{
	int count = 0;
	/* number of T cycles for each opcode (currently unused) */
	/* const int cycles[15] = {4, 10, 7, 6, 4, 4, 7, 4, 4, 11, 7, 6, 4, 4, 7}; */

	do {
		count++;

		/* call the interpreter on the next instruction */
		_instruction(cpu, memory);

		if (cpu->counter <= 0) {
			/* interrupt tasks here */
			cpu->counter += INTERRUPT_PERIOD;
		}

		if (!s_flag)
			runcycles--;

		/* step mode! */
		if (s_flag) {
			display_registers(cpu);
			display_bus(cpu->bus);
			display_mem(memory);
			fgetc(stdin);
		}

	} while (s_flag || (runcycles > 0));

	return count;
}
