/**
 * \file rotate.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_ROTATE__
#define __PZ80EMU_ROTATE__

#include <stdint.h>

#include "z80.h"

__BEGIN_DECLS
void _rlc(uint8_t, struct z80 *);
void _rrca(struct z80 *);
__END_DECLS

#endif
