/**
 * \file display.c
 * \brief display functions for curses management and displaying system info
 * \author Peter H. Ezetta
 * \date 2015-05-03
 */

#include <stdint.h>
#include <ncurses.h>

#include "z80.h"
#include "display.h"
#include "utils.h"

/**
 * \brief display the contents of a block of memory to a curses window
 * \param win The curses window in which to display memory.
 * \param memory A pointer to the block of memory to display
 * \todo check mvwprintw() return status
 * \todo check wprintw() return statuses
 * \todo check wrefresh() return status
 */
void
curses_display_mem(WINDOW *win, uint8_t *memory)
{
	int display_counter = 0;
	int rollover_counter = 0;
	int i;

	/* memory display routine */
	(void) mvwprintw(win, 1, 2, "Memory Display:");
	for (i = 0; i < 128; i++) {
		if (display_counter == 0)
			(void) mvwprintw(win, rollover_counter+2, 2, "0x%04hhX: ", i);

		(void)wprintw(win, "%02hhX ", memory[i]);
		display_counter++;

		if(display_counter == 16) {
			display_counter = 0;
			rollover_counter++;
			(void) wrefresh(win);
		}

		if(display_counter % 4 == 0)
			(void) wprintw(win, " ");
	}
}

/** 
 * \brief display the current register state to a curses window
 * \param win The curses window in which to display register state.
 * \param cpu A pointer to a z80 cpu struct for which to display registers.
 * \todo check return status of mvwprintw()
 * \todo check return status of wrefresh()
 */
void
curses_display_registers(WINDOW *win, struct z80 *cpu)
{
	/* register status display */
	(void) mvwprintw(win, 1, 2, "Register Status:");
	(void) mvwprintw(win, 2, 2, "pc\t  a\t  CNPHZS\t  bc\t  de\t  hl\t  ix\t  iy\t  'a\t  'CNPHZS\t  'bc\t  'de\t  'hl\t  sp\t  i\t  r");
	(void) mvwprintw(win, 3, 2, "%04x\t  %02hhX\t  %c%c%c%c%c%c\t  %04hX\t  %04x\t  %04hX\t  %04x\t  %04x\t  %02hhX\t   %c%c%c%c%c%c\t  %04x\t  %04x\t  %04X\t  %04x\t  %02X\t  %02X\t",
	    cpu->pc.W,
	    cpu->af.B.h,
	    (bool)IS_SET(cpu->af.B.l, CARRY) ? '1' : '0',
	    (bool)IS_SET(cpu->af.B.l, SUBTRACT) ? '1' : '0',
	    (bool)IS_SET(cpu->af.B.l, PARITY_OVERFLOW) ? '1' : '0',
	    (bool)IS_SET(cpu->af.B.l, HALF_CARRY) ? '1' : '0',
	    (bool)IS_SET(cpu->af.B.l, ZERO) ? '1' : '0',
	    (bool)IS_SET(cpu->af.B.l, SIGN) ? '1' : '0',
	    cpu->bc.W,
	    cpu->de.W,
	    cpu->hl.W,
	    cpu->ix.W,
	    cpu->iy.W,
	    cpu->_af.B.h,
	    (bool)IS_SET(cpu->_af.B.l, CARRY) ? '1' : '0',
	    (bool)IS_SET(cpu->_af.B.l, SUBTRACT) ? '1' : '0',
	    (bool)IS_SET(cpu->_af.B.l, PARITY_OVERFLOW) ? '1' : '0',
	    (bool)IS_SET(cpu->_af.B.l, HALF_CARRY) ? '1' : '0',
	    (bool)IS_SET(cpu->_af.B.l, ZERO) ? '1' : '0',
	    (bool)IS_SET(cpu->_af.B.l, SIGN) ? '1' : '0',
	    cpu->_bc.W,
	    cpu->_de.W,
	    cpu->_hl.W,
	    cpu->sp.W,
	    cpu->ir.B.h,
	    cpu->ir.B.l);
	(void) wrefresh(win);
}

/** 
 * \brief creates a new curses window
 * \param height The height of the window.
 * \param width The width of the window.
 * \param starty The starting y coordinate for the new window.
 * \param startx The starting x coordinate for the new window.
 * \return curses WINDOW
 * \todo check return value of box()
 * \todo check return value of wrefresh()
 */
WINDOW *
create_newwin(int height, int width, int starty, int startx)
{
	WINDOW *local_win;
	
	local_win = newwin(height, width, starty, startx);

	/* 0, 0 gives default characters for the vertical and horizontal lines */
	(void) box(local_win, 0, 0);
	(void) wrefresh(local_win);

	return local_win;
}

/**
 * \brief display the current memory contents to STDOUT
 * \param memory pointer to a memory for which to display registers.
 * \todo check return of printf()
 */
void
display_mem(uint8_t *memory)
{
	int display_counter = 0;
	int rollover_counter = 0;
	int i;
  
	/* memory display routine */
	(void) printf("Memory Display:\n");
	for (i = 0x0000; i < 0x0100; i++) {
		if (display_counter == 0)
			printf("0x%04X: ", i);
		
		printf("%02hhX ", memory[i]);
		display_counter++;
        
		if(display_counter == 16)
		{
			printf("\n");
			display_counter = 0;
			rollover_counter++;
			continue;
		}

		if(display_counter % 4 == 0)
			printf(" ");
	}
}

/**
 * \brief display the current register state to STDOUT
 * \param cpu A pointer to a z80 cpu struct for which to display registers.
 */
void
display_registers(struct z80 *cpu)
{
	/* register status display */
	printf("Register Status:\n");
	printf("pc\ta\t  CNPHZS\t  bc\t  de\t  hl\t  ix\t  iy\t  'a\t  'CNPHZS\t 'bc\t 'de\t 'hl\t  sp\t  i\t  r\n");
	printf("%04X\t%02hhX\t  %c%c%c%c%c%c\t  %04hX\t  %04X\t  %04hX\t  %04X\t  %04X\t  %02hhX\t   %c%c%c%c%c%c\t  %04X\t  %04X\t  %04X\t  %04X\t  %02X\t  %02X\n\n",
	    cpu->pc.W,
	    cpu->af.B.h,
	    (bool)IS_SET(cpu->af.B.l, CARRY) ? '1' : '0',
	    (bool)IS_SET(cpu->af.B.l, SUBTRACT) ? '1' : '0',
	    (bool)IS_SET(cpu->af.B.l, PARITY_OVERFLOW) ? '1' : '0',
	    (bool)IS_SET(cpu->af.B.l, HALF_CARRY) ? '1' : '0',
	    (bool)IS_SET(cpu->af.B.l, ZERO) ? '1' : '0',
	    (bool)IS_SET(cpu->af.B.l, SIGN) ? '1' : '0',
	    cpu->bc.W,
	    cpu->de.W,
	    cpu->hl.W,
	    cpu->ix.W,
	    cpu->iy.W,
	    cpu->_af.B.h,
	    (bool)IS_SET(cpu->_af.B.l, CARRY) ? '1' : '0',
	    (bool)IS_SET(cpu->_af.B.l, SUBTRACT) ? '1' : '0',
	    (bool)IS_SET(cpu->_af.B.l, PARITY_OVERFLOW) ? '1' : '0',
	    (bool)IS_SET(cpu->_af.B.l, HALF_CARRY) ? '1' : '0',
	    (bool)IS_SET(cpu->_af.B.l, ZERO) ? '1' : '0',
	    (bool)IS_SET(cpu->_af.B.l, SIGN) ? '1' : '0',
	    cpu->_bc.W,
	    cpu->_de.W,
	    cpu->_hl.W,
	    cpu->sp.W,
	    cpu->ir.B.h,
	    cpu->ir.B.l);
}

void
display_bus(struct bus *bus)
{
	printf("Bus Status:\n");
	printf("Address: %04hX\tData: %02hhX\n\n", bus->address, bus->data);
}
