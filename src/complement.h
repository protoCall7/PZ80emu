/**
 * \file complement.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_COMPLEMENT__
#define __PZ80EMU_COMPLEMENT__

#include "z80.h"

__BEGIN_DECLS
void _scf(struct z80 *);
void _ccf(struct z80 *);
__END_DECLS

#endif
