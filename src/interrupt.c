/**
 * \file interrupt.c
 */

#include "interrupt.h"
#include "z80.h"

/**
 * \brief disable interrupts
 * \param cpu z80 CPU object
 */
void
_di(struct z80 *cpu)
{
	cpu->iff1 = false;
	cpu->iff2 = false;
}

/**
 * \brief enable interrupts
 * \param cpu z80 CPU object
 */
void
_ei(struct z80 *cpu)
{
	cpu->iff1 = true;
	cpu->iff2 = true;
}
