/**
 * \file interpreter.c
 */

#include "../config.h"

#ifdef HAVE_ERR
# include <err.h>
#endif

#include "add.h"
#include "bcd.h"
#include "bit.h"
#include "boolean.h"
#include "complement.h"
#include "exchange.h"
#include "interpreter.h"
#include "interrupt.h"
#include "io.h"
#include "jump.h"
#include "load.h"
#include "rotate.h"
#include "stack.h"
#include "sub.h"
#include "utils.h"

/**
 * \brief parses an opcode from the BITS instruction set
 * \param cpu z80 CPU objects
 * \param memory pointer to allocated memory
 */
static void
__bits_instruction(struct z80 *cpu, uint8_t *memory)
{
	uint8_t opcode = memory[cpu->pc.W++];

	switch(opcode) {
		case 0x10: _rl(&cpu->bc.B.h, cpu); break; // rl b
		case 0x11: _rl(&cpu->bc.B.l, cpu); break; // rl c
		case 0x12: _rl(&cpu->de.B.h, cpu); break; // rl d
		case 0x13: _rl(&cpu->de.B.l, cpu); break; // rl e
		case 0x14: _rl(&cpu->hl.B.h, cpu); break; // rl h
		case 0x15: _rl(&cpu->hl.B.l, cpu); break; // rl l
		case 0x16: _rl(&memory[cpu->hl.W], cpu); break; // rl (hl)
		case 0x17: _rl(&cpu->af.B.h, cpu); break; // rl a
		case 0x20: _sla(&cpu->bc.B.h, cpu); break; // sla b
		case 0x21: _sla(&cpu->bc.B.l, cpu); break; // sla c
		case 0x22: _sla(&cpu->de.B.h, cpu); break; // sla d
		case 0x23: _sla(&cpu->de.B.l, cpu); break; // sla e
		case 0x24: _sla(&cpu->hl.B.h, cpu); break; // sla h
		case 0x25: _sla(&cpu->hl.B.l, cpu); break; // sla l
		case 0x26: _sla(&memory[cpu->hl.W], cpu); break; // sla (hl)
		case 0x27: _sla(&cpu->af.B.h, cpu); break; // sla a
		case 0x38: _srl(&cpu->bc.B.h, cpu); break; // srl b
		case 0x39: _srl(&cpu->bc.B.l, cpu); break; // srl c
		case 0x3A: _srl(&cpu->de.B.h, cpu); break; // srl d
		case 0x3B: _srl(&cpu->de.B.l, cpu); break; // srl e
		case 0x3C: _srl(&cpu->hl.B.h, cpu); break; // srl h
		case 0x3D: _srl(&cpu->hl.B.l, cpu); break; // srl l
		case 0x3E: _srl(&memory[cpu->hl.W], cpu); break; // srl (hl)
		case 0x3F: _srl(&cpu->af.B.h, cpu); break; // srl a
		case 0x40: _bit(cpu->bc.B.h, cpu, 0); break; // bit 0,b
		case 0x41: _bit(cpu->bc.B.l, cpu, 0); break; // bit 0,c
		case 0x42: _bit(cpu->de.B.h, cpu, 0); break; // bit 0,d
		case 0x43: _bit(cpu->de.B.l, cpu, 0); break; // bit 0,e
		case 0x44: _bit(cpu->hl.B.h, cpu, 0); break; // bit 0,h
		case 0x45: _bit(cpu->hl.B.l, cpu, 0); break; // bit 0,l
		case 0x46: _bit(memory[cpu->hl.W], cpu, 0); break; // bit 0,(hl)
		case 0x47: _bit(cpu->af.B.h, cpu, 0); break; // bit 0,a
		case 0x48: _bit(cpu->bc.B.h, cpu, 1); break; // bit 1,b
		case 0x49: _bit(cpu->bc.B.l, cpu, 1); break; // bit 1,c
		case 0x4A: _bit(cpu->de.B.h, cpu, 1); break; // bit 1,d
		case 0x4B: _bit(cpu->de.B.l, cpu, 1); break; // bit 1,e
		case 0x4C: _bit(cpu->hl.B.h, cpu, 1); break; // bit 1,h
		case 0x4D: _bit(cpu->hl.B.l, cpu, 1); break; // bit 1,l
		case 0x4E: _bit(memory[cpu->hl.W], cpu, 1); break; // bit 1,(hl)
		case 0x4F: _bit(cpu->af.B.h, cpu, 1); break; // bit 1,a
		case 0x50: _bit(cpu->bc.B.h, cpu, 2); break; // bit 2,b
		case 0x51: _bit(cpu->bc.B.l, cpu, 2); break; // bit 2,c
		case 0x52: _bit(cpu->de.B.h, cpu, 2); break; // bit 2,d
		case 0x53: _bit(cpu->de.B.l, cpu, 2); break; // bit 2,e
		case 0x54: _bit(cpu->hl.B.h, cpu, 2); break; // bit 2,h
		case 0x55: _bit(cpu->hl.B.l, cpu, 2); break; // bit 2,l
		case 0x56: _bit(memory[cpu->hl.W], cpu, 2); break; // bit 2,(hl)
		case 0x57: _bit(cpu->af.B.h, cpu, 2); break; // bit 2,a
		case 0x58: _bit(cpu->bc.B.h, cpu, 3); break; // bit 3,b
		case 0x59: _bit(cpu->bc.B.l, cpu, 3); break; // bit 3,c
		case 0x5A: _bit(cpu->de.B.h, cpu, 3); break; // bit 3,d
		case 0x5B: _bit(cpu->de.B.l, cpu, 3); break; // bit 3,e
		case 0x5C: _bit(cpu->hl.B.h, cpu, 3); break; // bit 3,h
		case 0x5D: _bit(cpu->hl.B.l, cpu, 3); break; // bit 3,l
		case 0x5E: _bit(memory[cpu->hl.W], cpu, 3); break; // bit 3,(hl)
		case 0x5F: _bit(cpu->af.B.h, cpu, 3); break; // bit 3,a
		case 0x60: _bit(cpu->bc.B.h, cpu, 4); break; // bit 4,b
		case 0x61: _bit(cpu->bc.B.l, cpu, 4); break; // bit 4,c
		case 0x62: _bit(cpu->de.B.h, cpu, 4); break; // bit 4,d
		case 0x63: _bit(cpu->de.B.l, cpu, 4); break; // bit 4,e
		case 0x64: _bit(cpu->hl.B.h, cpu, 4); break; // bit 4,h
		case 0x65: _bit(cpu->hl.B.l, cpu, 4); break; // bit 4,l
		case 0x66: _bit(memory[cpu->hl.W], cpu, 4); break; // bit 4,(hl)
		case 0x67: _bit(cpu->af.B.h, cpu, 4); break; // bit 4,a
		case 0x68: _bit(cpu->bc.B.h, cpu, 5); break; // bit 5,b
		case 0x69: _bit(cpu->bc.B.l, cpu, 5); break; // bit 5,c
		case 0x6A: _bit(cpu->de.B.h, cpu, 5); break; // bit 5,d
		case 0x6B: _bit(cpu->de.B.l, cpu, 5); break; // bit 5,e
		case 0x6C: _bit(cpu->hl.B.h, cpu, 5); break; // bit 5,h
		case 0x6D: _bit(cpu->hl.B.l, cpu, 5); break; // bit 5,l
		case 0x6E: _bit(memory[cpu->hl.W], cpu, 5); break; // bit 5,(hl)
		case 0x6F: _bit(cpu->af.B.h, cpu, 5); break; // bit 5,a
		case 0x70: _bit(cpu->bc.B.h, cpu, 6); break; // bit 6,b
		case 0x71: _bit(cpu->bc.B.l, cpu, 6); break; // bit 6,c
		case 0x72: _bit(cpu->de.B.h, cpu, 6); break; // bit 6,d
		case 0x73: _bit(cpu->de.B.l, cpu, 6); break; // bit 6,e
		case 0x74: _bit(cpu->hl.B.h, cpu, 6); break; // bit 6,h
		case 0x75: _bit(cpu->hl.B.l, cpu, 6); break; // bit 6,l
		case 0x76: _bit(memory[cpu->hl.W], cpu, 6); break; // bit 6,(hl)
		case 0x77: _bit(cpu->af.B.h, cpu, 6); break; // bit 6,a
		case 0x78: _bit(cpu->bc.B.h, cpu, 7); break; // bit 7,b
		case 0x79: _bit(cpu->bc.B.l, cpu, 7); break; // bit 7,c
		case 0x7A: _bit(cpu->de.B.h, cpu, 7); break; // bit 7,d
		case 0x7B: _bit(cpu->de.B.l, cpu, 7); break; // bit 7,e
		case 0x7C: _bit(cpu->hl.B.h, cpu, 7); break; // bit 7,h
		case 0x7D: _bit(cpu->hl.B.l, cpu, 7); break; // bit 7,l
		case 0x7E: _bit(memory[cpu->hl.W], cpu, 7); break; // bit 7,(hl)
		case 0x7F: _bit(cpu->af.B.h, cpu, 7); break; // bit 7,a
		default:
			errx(1, "Invalid Instruction");
	}
}

/**
 * \brief parses an opcode from the IX instruction set
 * \param cpu z80 CPU objects
 * \param memory pointer to allocated memory
 */
static void
__ix_instruction(struct z80 *cpu, uint8_t *memory)
{
	uint8_t opcode = memory[cpu->pc.W++];

	switch(opcode) {
		case 0x21: _load_reg16_nn(&cpu->ix, memory, &cpu->pc); break; // ld ix,nn
		case 0x22: _load_mem_reg16(&cpu->ix, memory, &cpu->pc); break; // ld (nn),ix
		case 0x2A: _load_reg16_mem(&cpu->ix, memory, &cpu->pc); break; // ld ix,(nn)
		case 0x36: _load_idx_offset_n(&cpu->ix, memory, &cpu->pc); break; // ld (ix+n),n
		case 0x39: _add_reg16_reg16(&cpu->ix, &cpu->sp, cpu); break; // add ix,sp
		case 0x46: _load_reg8_mem_idx_offset(&cpu->bc.B.h, &cpu->ix, memory, &cpu->pc); break; // ld b,(ix+n)
		case 0x4E: _load_reg8_mem_idx_offset(&cpu->bc.B.l, &cpu->ix, memory, &cpu->pc); break; // ld c,(ix+n)
		case 0x56: _load_reg8_mem_idx_offset(&cpu->de.B.h, &cpu->ix, memory, &cpu->pc); break; // ld d,(ix+n)
		case 0x5E: _load_reg8_mem_idx_offset(&cpu->de.B.l, &cpu->ix, memory, &cpu->pc); break; // ld e,(ix+n)
		case 0x66: _load_reg8_mem_idx_offset(&cpu->hl.B.h, &cpu->ix, memory, &cpu->pc); break; // ld h,(ix+n)
		case 0x6E: _load_reg8_mem_idx_offset(&cpu->hl.B.l, &cpu->ix, memory, &cpu->pc); break; // ld l,(ix+n)
		case 0x70: _load_mem_idx_offset_reg8(&cpu->bc.B.h, &cpu->ix, memory, &cpu->pc); break; // ld (ix+n),b
		case 0x71: _load_mem_idx_offset_reg8(&cpu->bc.B.l, &cpu->ix, memory, &cpu->pc); break; // ld (ix+n),c
		case 0x72: _load_mem_idx_offset_reg8(&cpu->de.B.h, &cpu->ix, memory, &cpu->pc); break; // ld (ix+n),d
		case 0x73: _load_mem_idx_offset_reg8(&cpu->de.B.l, &cpu->ix, memory, &cpu->pc); break; // ld (ix+n),e
		case 0x74: _load_mem_idx_offset_reg8(&cpu->hl.B.h, &cpu->ix, memory, &cpu->pc); break; // ld (ix+n),h
		case 0x75: _load_mem_idx_offset_reg8(&cpu->hl.B.l, &cpu->ix, memory, &cpu->pc); break; // ld (ix+n),l
		case 0x77: _load_mem_idx_offset_reg8(&cpu->af.B.h, &cpu->ix, memory, &cpu->pc); break; // ld (ix+n),a
		case 0x7E: _load_reg8_mem_idx_offset(&cpu->af.B.h, &cpu->ix, memory, &cpu->pc); break; // ld a,(ix+n)
		case 0xE3: _ex_sp_reg16(&cpu->ix, cpu, memory); break; // ex (sp),ix
		case 0xE5: _push(&cpu->ix, cpu, memory); break; // push ix
		default:
			   errx(1, "Invalid Instruction");
	}
}

/**
 * \brief parses an opcode from the IY instruction set
 * \param cpu z80 CPU objects
 * \param memory pointer to allocated memory
 */
static void
__iy_instruction(struct z80 *cpu, uint8_t *memory)
{
	uint8_t opcode = memory[cpu->pc.W++];

	switch(opcode) {
		case 0x09: break; // add iy,bc
		case 0x19: break; // add iy,de
		case 0x21: _load_reg16_nn(&cpu->iy, memory, &cpu->pc); break; // ld iy,nn
		case 0x22: _load_mem_reg16(&cpu->iy, memory, &cpu->pc); break; // ld (nn),iy
		case 0x23: break; // inc iy
		case 0x2A: _load_reg16_mem(&cpu->iy, memory, &cpu->pc); break; // ld iy,(nn)
		case 0x36: _load_idx_offset_n(&cpu->iy, memory, &cpu->pc); break; // ld (iy+n),n
		case 0x46: _load_reg8_mem_idx_offset(&cpu->bc.B.h, &cpu->iy, memory, &cpu->pc); break; // ld b,(iy+n)
		case 0x4E: _load_reg8_mem_idx_offset(&cpu->bc.B.l, &cpu->iy, memory, &cpu->pc); break; // ld c,(iy+n)
		case 0x56: _load_reg8_mem_idx_offset(&cpu->de.B.h, &cpu->iy, memory, &cpu->pc); break; // ld d,(iy+n)
		case 0x5E: _load_reg8_mem_idx_offset(&cpu->de.B.l, &cpu->iy, memory, &cpu->pc); break; // ld e,(iy+n)
		case 0x66: _load_reg8_mem_idx_offset(&cpu->hl.B.h, &cpu->iy, memory, &cpu->pc); break; // ld h,(iy+n)
		case 0x6E: _load_reg8_mem_idx_offset(&cpu->hl.B.l, &cpu->iy, memory, &cpu->pc); break; // ld l,(iy+n)
		case 0x70: _load_mem_idx_offset_reg8(&cpu->bc.B.h, &cpu->iy, memory, &cpu->pc); break; // ld (iy+n),b
		case 0x71: _load_mem_idx_offset_reg8(&cpu->bc.B.l, &cpu->iy, memory, &cpu->pc); break; // ld (iy+n),c
		case 0x72: _load_mem_idx_offset_reg8(&cpu->de.B.h, &cpu->iy, memory, &cpu->pc); break; // ld (iy+n),d
		case 0x73: _load_mem_idx_offset_reg8(&cpu->de.B.l, &cpu->iy, memory, &cpu->pc); break; // ld (iy+n),e
		case 0x74: _load_mem_idx_offset_reg8(&cpu->hl.B.h, &cpu->iy, memory, &cpu->pc); break; // ld (iy+n),h
		case 0x75: _load_mem_idx_offset_reg8(&cpu->hl.B.l, &cpu->iy, memory, &cpu->pc); break; // ld (iy+n),l
		case 0x77: _load_mem_idx_offset_reg8(&cpu->af.B.h, &cpu->iy, memory, &cpu->pc); break; // ld (iy+n),a
		case 0x7E: _load_reg8_mem_idx_offset(&cpu->af.B.h, &cpu->iy, memory, &cpu->pc); break; // ld a,(iy+n)
		case 0xE3: _ex_sp_reg16(&cpu->iy, cpu, memory); break; // ex (sp),iy
		default:
			errx(1, "Invalid Instruction");
	}
}

/**
 * \brief parses an opcode from the extended instruction set
 * \param cpu z80 CPU objects
 * \param memory pointer to allocated memory
 */
static void
__extended_instruction(struct z80 *cpu, uint8_t *memory)
{
	uint8_t opcode = memory[cpu->pc.W++];

	switch(opcode) {
		case 0x41: _out_c_reg8(cpu->bc.B.h, cpu, memory); break; // out (c),b
		case 0x43: _load_mem_reg16(&cpu->bc, memory, &cpu->pc); break; // ld (nn),bc
		case 0x4A: _add_reg16_reg16(&cpu->hl, &cpu->bc, cpu); break; // adc hl,bc
		case 0x4B: _load_reg16_mem(&cpu->bc, memory, &cpu->pc); break; // ld bc,(nn)
		case 0x47: cpu->ir.B.h = cpu->af.B.h; break; // ld i,a
		case 0x4F: cpu->ir.B.l = cpu->af.B.h; break; // ld r,a
		case 0x52: cpu->hl.W -= (cpu->de.W - IS_SET(cpu->af.B.l, CARRY)); break; // sbc hl,de
		case 0x53: _load_mem_reg16(&cpu->de, memory, &cpu->pc); break; // ld (nn),de
		case 0x57: cpu->af.B.h = cpu->ir.B.h; break; // ld a,i
		case 0x5B: _load_reg16_mem(&cpu->de, memory, &cpu->pc); break; // ld de,(nn)
		case 0x5E: break; // im 2
		case 0x5F: cpu->af.B.h = cpu->ir.B.l; break; // ld a,r
		case 0x61: _out_c_reg8(cpu->hl.B.h, cpu, memory); break; // out (c),h
		case 0x73: _load_mem_reg16(&cpu->sp, memory, &cpu->pc); break; // ld (nn),sp
		case 0x79: _out_c_reg8(cpu->af.B.h, cpu, memory); break; // out (c),a
		case 0x7B: _load_reg16_mem(&cpu->sp, memory, &cpu->pc); break; // ld sp,(nn)
		case 0x7E: break; // im 2
		case 0xB3: _otir(cpu, memory); break;
		default:
			errx(1, "Invalid Instruction");
	}
}

/**
 * \brief parses an opcode from the main instruction set
 * \param cpu z80 CPU objects
 * \param memory pointer to allocated memory
 */
void
_instruction(struct z80 *cpu, uint8_t *memory)
{
	uint8_t opcode = memory[cpu->pc.W++]; // fetch next opcode from memory
	// cpu->counter -= cycles[opcode]; // decrease the interrupt counter by number of cycles for opcode

	switch (opcode) {
		case 0x00: break; // nop
		case 0x01: _load_reg16_nn(&cpu->bc, memory, &cpu->pc); break; // ld bc,nn
		case 0x02: memory[cpu->bc.W] = cpu->af.B.h; break; // ld (bc),a
		case 0x03: cpu->bc.W++; break; // inc bc
		case 0x04: _inc8(&cpu->bc.B.h, cpu); break; // inc b
		case 0x05: _dec8(&cpu->bc.B.h, cpu); break; // dec b
		case 0x06: cpu->bc.B.h = memory[cpu->pc.W++]; break; // ld b,n
		case 0x07: _rlc(cpu->af.B.h, cpu); break; // rlca
		case 0x08: _ex_reg16(&cpu->af, &cpu->_af); break; // ex af,af'
		case 0x09: _add_reg16_reg16(&cpu->hl, &cpu->bc, cpu); break; // add hl,bc
		case 0x0A: cpu->af.B.h = memory[cpu->bc.W]; break; // ld a,(bc)
		case 0x0B: cpu->bc.W--; break; // dec bc
		case 0x0C: _inc8(&cpu->bc.B.l, cpu); break; // inc c
		case 0x0D: _dec8(&cpu->bc.B.l, cpu); break; // dec c
		case 0x0E: cpu->bc.B.l = memory[cpu->pc.W++]; break; // ld c,n
		case 0x0F: _rrca(cpu); break; // rrca
		case 0x10: _djnz_n(cpu, memory); break; // djnz n
		case 0x11: _load_reg16_nn(&cpu->de, memory, &cpu->pc); break; // ld de,nn
		case 0x12: memory[cpu->de.W] = cpu->af.B.h; break; // ld (de),a
		case 0x13: cpu->de.W++; break; // inc de
		case 0x14: _inc8(&cpu->de.B.h, cpu); break; // inc d
		case 0x15: _dec8(&cpu->de.B.h, cpu); break; // dec d
		case 0x16: cpu->de.B.h = memory[cpu->pc.W++]; break; // ld d,n
		case 0x17: break; // rla: WIP
		case 0x18: {
				   // jr n
				   uint8_t offset = cpu->pc.W++;
				   cpu->pc.W += (int8_t) memory[offset];
				   break; 
			   }
		case 0x19: _add_reg16_reg16(&cpu->hl, &cpu->de, cpu); break; // add hl,de
		case 0x1A: cpu->af.B.h = memory[cpu->de.W]; break; // ld a,(de)
		case 0x1B: cpu->de.W--; break; // dec de
		case 0x1C: _inc8(&cpu->de.B.l, cpu); break; // inc e
		case 0x1D: _dec8(&cpu->de.B.l, cpu); break; // dec e
		case 0x1E: cpu->de.B.l = memory[cpu->pc.W++]; break; // ld e,n
		case 0x1F: break; // rrca: WIP
		case 0x20: _jr(cpu, memory, ZERO, true); break; // jr nz,n
		case 0x21: _load_reg16_nn(&cpu->hl, memory, &cpu->pc); break; // ld hl,nn
		case 0x22: _load_mem_reg16(&cpu->hl, memory, &cpu->pc); break; // ld (nn),hl
		case 0x23: cpu->hl.W++; break; // inc hl
		case 0x24: _inc8(&cpu->hl.B.h, cpu); break; // inc h
		case 0x25: _dec8(&cpu->hl.B.h, cpu); break; // dec h
		case 0x26: cpu->hl.B.h = memory[cpu->pc.W++]; break; // ld h,n
		case 0x27: _daa(cpu); break; // daa
		case 0x28: _jr(cpu, memory, ZERO, false); break; // jr z,n
		case 0x29: _add_reg16_reg16(&cpu->hl, &cpu->hl, cpu); break; // add hl,hl
		case 0x2A: _load_reg16_mem(&cpu->hl, memory, &cpu->pc); break; // ld hl,(nn)
		case 0x2B: cpu->hl.W--; break; // dec hl
		case 0x2C: _inc8(&cpu->hl.B.l, cpu); break; // inc l
		case 0x2D: _dec8(&cpu->hl.B.l, cpu); break; // dec l
		case 0x2E: cpu->hl.B.l = memory[cpu->pc.W++]; break; // ld l,n
		case 0x2F: break; // rra: WIP
		case 0x30: _jr(cpu, memory, CARRY, true); break; // jr nc,n
		case 0x31: _load_reg16_nn(&cpu->sp, memory, &cpu->pc); break; // ld sp,nn
		case 0x32: _load_mem_a(cpu, memory); break; // ld (nn),a
		case 0x33: cpu->sp.W++; break; // inc sp
		case 0x34: _inc8(&memory[cpu->hl.W], cpu); break; // inc (hl)
		case 0x35: _dec8(&memory[cpu->hl.W], cpu); break; // dec (hl)
		case 0x36: memory[cpu->hl.W] = memory[cpu->pc.W++]; break; // ld (hl),n
		case 0x37: _scf(cpu); break; // scf
		case 0x38: _jr(cpu, memory, CARRY, false); break; // jr c,n
		case 0x39: _add_reg16_reg16(&cpu->hl, &cpu->sp, cpu); break; // add hl,sp
		case 0x3A: _load_a_mem(cpu, memory); break; // ld a,(nn)
		case 0x3B: cpu->sp.W--; break; // dec sp
		case 0x3C: _inc8(&cpu->af.B.h, cpu); break; // inc a
		case 0x3D: _dec8(&cpu->af.B.h, cpu); break; // dec a
		case 0x3E: cpu->af.B.h = memory[cpu->pc.W++]; break; // ld a,n
		case 0x3F: _ccf(cpu); break; // ccf
		case 0x40: cpu->bc.B.h = cpu->bc.B.h; break; // ld b,b
		case 0x41: cpu->bc.B.h = cpu->bc.B.l; break; // ld b,c
		case 0x42: cpu->bc.B.h = cpu->de.B.h; break; // ld b,d
		case 0x43: cpu->bc.B.h = cpu->de.B.l; break; // ld b,e
		case 0x44: cpu->bc.B.h = cpu->hl.B.h; break; // ld b,h
		case 0x45: cpu->bc.B.h = cpu->hl.B.l; break; // ld b,l
		case 0x46: cpu->bc.B.h = memory[cpu->hl.W];; break; // ld b,(hl)
		case 0x47: cpu->bc.B.h = cpu->af.B.h; break; // ld b,a
		case 0x48: cpu->bc.B.l = cpu->bc.B.h; break; // ld c,b
		case 0x49: cpu->bc.B.l = cpu->bc.B.l; break; // ld c,c
		case 0x4A: cpu->bc.B.l = cpu->de.B.h; break; // ld c,d
		case 0x4B: cpu->bc.B.l = cpu->de.B.l; break; // ld c,e
		case 0x4C: cpu->bc.B.l = cpu->hl.B.h; break; // ld c,h
		case 0x4D: cpu->bc.B.l = cpu->hl.B.l; break; // ld c,l
		case 0x4E: cpu->bc.B.l = memory[cpu->hl.W]; break; // ld c,(hl)
		case 0x4F: cpu->bc.B.l = cpu->af.B.h; break; // ld c,a
		case 0x50: cpu->de.B.h = cpu->bc.B.h; break; // ld d,b
		case 0x51: cpu->de.B.h = cpu->bc.B.l; break; // ld d,c
		case 0x52: cpu->de.B.h = cpu->de.B.h; break; // ld d,d
		case 0x53: cpu->de.B.h = cpu->de.B.l; break; // ld d,e
		case 0x54: cpu->de.B.h = cpu->hl.B.h; break; // ld d,h
		case 0x55: cpu->de.B.h = cpu->hl.B.l; break; // ld d,l
		case 0x56: cpu->de.B.h = memory[cpu->hl.W]; break; // ld d,(hl)
		case 0x57: cpu->de.B.h = cpu->af.B.h; break; // ld d,a
		case 0x58: cpu->de.B.l = cpu->bc.B.h; break; // ld e,b 
		case 0x59: cpu->de.B.l = cpu->bc.B.l; break; // ld e,c
		case 0x5A: cpu->de.B.l = cpu->de.B.h; break; // ld e,d
		case 0x5B: cpu->de.B.l = cpu->de.B.l; break; // ld e,e
		case 0x5C: cpu->de.B.l = cpu->hl.B.h; break; // ld e,h
		case 0x5D: cpu->de.B.l = cpu->hl.B.l; break; // ld e,l
		case 0x5E: cpu->de.B.l = memory[cpu->hl.W]; break; // ld e,(hl)
		case 0x5F: cpu->de.B.l = cpu->af.B.h; break; // ld e,a
		case 0x60: cpu->hl.B.h = cpu->bc.B.h; break; // ld h,b
		case 0x61: cpu->hl.B.h = cpu->bc.B.l; break; // ld h,c
		case 0x62: cpu->hl.B.h = cpu->de.B.h; break; // ld h,d
		case 0x63: cpu->hl.B.h = cpu->de.B.l; break; // ld h,e
		case 0x64: cpu->hl.B.h = cpu->hl.B.h; break; // ld h,h
		case 0x65: cpu->hl.B.h = cpu->hl.B.l; break; // ld h,l
		case 0x66: cpu->hl.B.h = memory[cpu->hl.W]; break; // ld h,(hl)
		case 0x67: cpu->hl.B.h = cpu->af.B.h; break; // ld h,a
		case 0x68: cpu->hl.B.l = cpu->bc.B.h; break; // ld l,b
		case 0x69: cpu->hl.B.l = cpu->bc.B.l; break; // ld l,c
		case 0x6A: cpu->hl.B.l = cpu->de.B.h; break; // ld l,d
		case 0x6B: cpu->hl.B.l = cpu->de.B.l; break; // ld l,e
		case 0x6C: cpu->hl.B.l = cpu->hl.B.h; break; // ld l,h
		case 0x6D: cpu->hl.B.l = cpu->hl.B.l; break; // ld l,l
		case 0x6E: cpu->hl.B.l = memory[cpu->hl.W]; break; // ld l,(hl)
		case 0x6F: cpu->hl.B.l = cpu->af.B.h; break; // ld l,a
		case 0x70: memory[cpu->hl.W] = cpu->bc.B.h; break; // ld (hl),b
		case 0x71: memory[cpu->hl.W] = cpu->bc.B.l; break; // ld (hl),c
		case 0x72: memory[cpu->hl.W] = cpu->de.B.h; break; // ld (hl),d
		case 0x73: memory[cpu->hl.W] = cpu->de.B.l; break; // ld (hl),e
		case 0x74: memory[cpu->hl.W] = cpu->hl.B.h; break; // ld (hl),h
		case 0x75: memory[cpu->hl.W] = cpu->hl.B.l; break; // ld (hl),l
		case 0x76: errx(-127, "Halted"); break; // halt
		case 0x77: memory[cpu->hl.W] = cpu->af.B.h; break; // ld (hl),a
		case 0x78: cpu->af.B.h = cpu->bc.B.h; break; // ld a,b
		case 0x79: cpu->af.B.h = cpu->bc.B.l; break; // ld a,c
		case 0x7A: cpu->af.B.h = cpu->de.B.h; break; // ld a,d
		case 0x7B: cpu->af.B.h = cpu->de.B.l; break; // ld a,e
		case 0x7C: cpu->af.B.h = cpu->hl.B.h; break; // ld a,h
		case 0x7D: cpu->af.B.h = cpu->hl.B.l; break; // ld a,l
		case 0x7E: cpu->af.B.h = memory[cpu->hl.W]; break; // ld a,(hl)
		case 0x7F: cpu->af.B.h = cpu->af.B.h; break; // ld a,a
		case 0x80: _add_a_reg8(cpu, &cpu->bc.B.h); break; // add a,b
		case 0x81: _add_a_reg8(cpu, &cpu->bc.B.l); break; // add a,c
		case 0x82: _add_a_reg8(cpu, &cpu->de.B.h); break; // add a,d
		case 0x83: _add_a_reg8(cpu, &cpu->de.B.l); break; // add a,e
		case 0x84: _add_a_reg8(cpu, &cpu->hl.B.h); break; // add a,h
		case 0x85: _add_a_reg8(cpu, &cpu->hl.B.l); break; // add a,l
		case 0x86: _add_a_reg8(cpu, &memory[cpu->hl.W]); break; // add a,(hl)
		case 0x87: _add_a_reg8(cpu, &cpu->af.B.h); break; // add a,a
		case 0x88: _adc_a_reg8(cpu, &cpu->bc.B.h); break; // adc a,b
		case 0x89: _adc_a_reg8(cpu, &cpu->bc.B.l); break; // adc a,c
		case 0x8A: _adc_a_reg8(cpu, &cpu->de.B.h); break; // adc a,d
		case 0x8B: _adc_a_reg8(cpu, &cpu->de.B.l); break; // adc a,e
		case 0x8C: _adc_a_reg8(cpu, &cpu->hl.B.h); break; // adc a,h
		case 0x8D: _adc_a_reg8(cpu, &cpu->hl.B.l); break; // adc a,l
		case 0x8E: _adc_a_reg8(cpu, &memory[cpu->hl.W]); break; // adc a,(hl)
		case 0x8F: _adc_a_reg8(cpu, &cpu->af.B.h); break; // adc a,a
		case 0x90: _sub_a_reg8(cpu, &cpu->bc.B.h); break; // sub b
		case 0x91: _sub_a_reg8(cpu, &cpu->bc.B.l); break; // sub c
		case 0x92: _sub_a_reg8(cpu, &cpu->de.B.h); break; // sub d
		case 0x93: _sub_a_reg8(cpu, &cpu->de.B.l); break; // sub e
		case 0x94: _sub_a_reg8(cpu, &cpu->hl.B.h); break; // sub h
		case 0x95: _sub_a_reg8(cpu, &cpu->hl.B.l); break; // sub l
		case 0x96: _sub_a_reg8(cpu, &memory[cpu->hl.W]); break; // sub (hl)
		case 0x97: _sub_a_reg8(cpu, &cpu->af.B.h); break; // sub a
		case 0x98: _sbc_a_reg8(cpu, &cpu->bc.B.h); break; // sbc a,b
		case 0x99: _sbc_a_reg8(cpu, &cpu->bc.B.l); break; // sbc a,c
		case 0x9A: _sbc_a_reg8(cpu, &cpu->de.B.h); break; // sbc a,d
		case 0x9B: _sbc_a_reg8(cpu, &cpu->de.B.l); break; // sbc a,e
		case 0x9C: _sbc_a_reg8(cpu, &cpu->hl.B.h); break; // sbc a,h
		case 0x9D: _sbc_a_reg8(cpu, &cpu->hl.B.l); break; // sbc a,l
		case 0x9E: cpu->af.B.h = (cpu->af.B.h - memory[cpu->hl.W] - IS_SET(cpu->af.B.l, CARRY)); break; // sbc a,(hl)
		case 0x9F: _sbc_a_reg8(cpu, &cpu->af.B.h); break; // sbc a,a
		case 0xA0: _and(&cpu->bc.B.h, cpu); break; // and b
		case 0xA1: _and(&cpu->bc.B.l, cpu); break; // and c
		case 0xA2: _and(&cpu->de.B.h, cpu); break; // and d
		case 0xA3: _and(&cpu->de.B.l, cpu); break; // and e
		case 0xA4: _and(&cpu->hl.B.h, cpu); break; // and h
		case 0xA5: _and(&cpu->hl.B.l, cpu); break; // and l
		case 0xA6: _and(&memory[cpu->hl.W], cpu); break; // and (hl)
		case 0xA7: _and(&cpu->af.B.h, cpu); break; // and a
		case 0xA8: _xor(&cpu->bc.B.h, cpu); break; // xor b
		case 0xA9: _xor(&cpu->bc.B.l, cpu); break; // xor c
		case 0xAA: _xor(&cpu->de.B.h, cpu); break; // xor d
		case 0xAB: _xor(&cpu->de.B.l, cpu); break; // xor e
		case 0xAC: _xor(&cpu->hl.B.h, cpu); break; // xor h
		case 0xAD: _xor(&cpu->hl.B.l, cpu); break; // xor l
		case 0xAE: _xor(&memory[cpu->hl.W], cpu); break; // xor (hl)
		case 0xAF: _xor(&cpu->af.B.h, cpu); break; // xor a
		case 0xB0: _or(&cpu->bc.B.h, cpu); break; // or b
		case 0xB1: _or(&cpu->bc.B.l, cpu); break; // or c
		case 0xB2: _or(&cpu->de.B.h, cpu); break; // or d
		case 0xB3: _or(&cpu->de.B.l, cpu); break; // or e
		case 0xB4: _or(&cpu->hl.B.h, cpu); break; // or h
		case 0xB5: _or(&cpu->hl.B.l, cpu); break; // or l
		case 0xB6: _or(&memory[cpu->hl.W], cpu); break; // or (hl)
		case 0xB7: _or(&cpu->af.B.h, cpu); break; // or a
		case 0xB8: _cp(&cpu->bc.B.h, cpu); break; // cp b
		case 0xB9: _cp(&cpu->bc.B.l, cpu); break; // cp c
		case 0xBA: _cp(&cpu->de.B.h, cpu); break; // cp d
		case 0xBB: _cp(&cpu->de.B.l, cpu); break; // cp e
		case 0xBC: _cp(&cpu->hl.B.h, cpu); break; // cp h
		case 0xBD: _cp(&cpu->hl.B.l, cpu); break; // cp l
		case 0xBE: _cp(&memory[cpu->hl.W], cpu); break; // cp (hl)
		case 0xBF: _cp(&cpu->af.B.h, cpu); break; // cp a
		case 0xC0: _ret_flag(cpu, memory, ZERO, true); break; // ret nz
		case 0xC1: _pop(&cpu->bc, cpu, memory); break; // pop bc
		case 0xC2: _jp_flag_nn(cpu, memory, ZERO, true); break; // jp nz,nn
		case 0xC3: _jp_nn(cpu, memory); break; // jp nn
		case 0xC4: _call_flag_nn(cpu, memory, ZERO, true); break; // call nz,nn
		case 0xC5: _push(&cpu->bc, cpu, memory); break; // push bc
		case 0xC6: _add_a_mem(cpu, memory); break; // add n
		case 0xC7: _rst(cpu, memory, 0x0000); break; // rst 0x00
		case 0xC8: _ret_flag(cpu, memory, ZERO, false); break; // ret z
		case 0xC9: _ret(cpu, memory); break; // ret
		case 0xCA: _jp_flag_nn(cpu, memory, ZERO, false); break; // jp z,nn
		case 0xCB: __bits_instruction(cpu, memory); break; // BITS instruction set
		case 0xCC: _call_flag_nn(cpu, memory, ZERO, false); break; // call z,nn
		case 0xCD: _call_nn(cpu, memory); break; // call nn
		case 0xCE: _adc_a_mem(cpu, memory); break; // adc a,n
		case 0xCF: _rst(cpu, memory, 0x0008); break; // rst 0x08
		case 0xD0: _ret_flag(cpu, memory, CARRY, true); break; // ret nc
		case 0xD1: _pop(&cpu->de, cpu, memory); break; // pop de
		case 0xD2: _jp_flag_nn(cpu, memory, CARRY, true); break; // jp nc,nn
		case 0xD3: _out_mem_reg8(cpu->af.B.h, cpu, memory); break; // out (nn),a
		case 0xD4: _call_flag_nn(cpu, memory, CARRY, true); break; // call nc,nn
		case 0xD5: _push(&cpu->de, cpu, memory); break; // push de
		case 0xD6: _sub_a_mem(cpu, memory); break; // sub *
		case 0xD7: _rst(cpu, memory, 0x0010); break; // rst 0x10
		case 0xD8: _ret_flag(cpu, memory, CARRY, false); break; // ret c
		case 0xD9: _exx(cpu); break; // exx
		case 0xDA: _jp_flag_nn(cpu, memory, CARRY, false); break; // jp c,nn
		case 0xDB: _in_reg8_mem(&cpu->af.B.h, cpu, memory); break; // in a,(nn)
		case 0xDC: _call_flag_nn(cpu, memory, CARRY, false); break; // call c,nn
		case 0xDD: __ix_instruction(cpu, memory); break; // IX instruction set
		case 0xDE: break;
		case 0xDF: _rst(cpu, memory, 0x0018); break; // rst 0x18
		case 0xE0: _ret_flag(cpu, memory, PARITY_OVERFLOW, true); break; // ret po
		case 0xE1: _pop(&cpu->hl, cpu, memory); break; // pop hl
		case 0xE2: _jp_flag_nn(cpu, memory, PARITY_OVERFLOW, true); break; // jp po,nn
		case 0xE3: _ex_sp_reg16(&cpu->hl, cpu, memory); break; // ex (sp),hl
		case 0xE4: _call_flag_nn(cpu, memory, PARITY_OVERFLOW, true); break; // call po,nn
		case 0xE5: _push(&cpu->hl, cpu, memory); break; // push hl
		case 0xE6: _and(&memory[cpu->pc.W++], cpu); break; // and n
		case 0xE7: _rst(cpu, memory, 0x0020); break; // rst 0x20
		case 0xE8: _ret_flag(cpu, memory, PARITY_OVERFLOW, false); break; // ret pe
		case 0xE9: _jp_reg(&cpu->hl, cpu); break; // jp (hl)
		case 0xEA: _jp_flag_nn(cpu, memory, PARITY_OVERFLOW, false); break; // jp pe,nn
		case 0xEB: _ex_reg16(&cpu->hl, &cpu->de); break; // ex hl,de
		case 0xEC: _call_flag_nn(cpu, memory, PARITY_OVERFLOW, false); break; // call pe,nn
		case 0xED: __extended_instruction(cpu, memory); break; // Extended instruction set
		case 0xEE: _xor(&memory[cpu->pc.W++], cpu); break; // xor n
		case 0xEF: _rst(cpu, memory, 0x0028); break; // rst 0x28
		case 0xF0: _ret_flag(cpu, memory, SIGN, true); break; // ret p
		case 0xF1: _pop(&cpu->af, cpu, memory); break; // pop af
		case 0xF2: _jp_flag_nn(cpu, memory, SIGN, true); break; // jp p,nn
		case 0xF3: _di(cpu); break; // di
		case 0xF4: _call_flag_nn(cpu, memory, SIGN, true); break; // call p,nn
		case 0xF5: _push(&cpu->af, cpu, memory); break; // push af
		case 0xF6: _or(&memory[cpu->pc.W++], cpu); break; // or n
		case 0xF7: _rst(cpu, memory, 0x0030); break; // rst 0x30
		case 0xF8: _ret_flag(cpu, memory, SIGN, false); break; // ret m
		case 0xF9: cpu->sp.W = cpu->hl.W; break; // ld sp,hl
		case 0xFA: _jp_flag_nn(cpu, memory, SIGN, false); break; // jp m,nn
		case 0xFB: _ei(cpu); break; // ei
		case 0xFC: _call_flag_nn(cpu, memory, SIGN, false); break; // call m,nn
		case 0xFD: __iy_instruction(cpu, memory); break; // IY instruction set
		case 0xFE: _cp(&memory[cpu->pc.W++], cpu); break; // cp n
		case 0xFF: _rst(cpu, memory, 0x0038); break; // rst 0x38
		default:
			errx(1, "Invalid Instruction");
	}
}
