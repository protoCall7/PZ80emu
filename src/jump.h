/**
 * \file jump.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_JUMP__
#define __PZ80EMU_JUMP__

#include <stdbool.h>
#include <stdint.h>

#include "z80.h"

__BEGIN_DECLS
void _djnz_n(struct z80 *, uint8_t *);
void _jr(struct z80 *, uint8_t *, int, bool);
void _jp_nn(struct z80 *, uint8_t *);
void _jp_flag_nn(struct z80 *, uint8_t *, int, bool);
void _jp_reg(word *, struct z80 *);
__END_DECLS

#endif
