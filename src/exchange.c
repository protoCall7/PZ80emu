/**
 * \file exchange.c
 */

#include "exchange.h"

/**
 * exchange two 16-bit registers
 * \param reg1 first register
 * \param reg2 second register
 */
void
_ex_reg16(word *reg1, word *reg2)
{
	if (reg1->W != reg2->W) {
		reg1->W ^= reg2->W;
		reg2->W ^= reg1->W;
		reg1->W ^= reg2->W;
	}
}

/**
 * exchange bc, de, and hl register pairs with their alternates
 * \param cpu z80 cpu objects
 */
void
_exx(struct z80 *cpu)
{
	_ex_reg16(&cpu->bc, &cpu->_bc);
	_ex_reg16(&cpu->de, &cpu->_de);
	_ex_reg16(&cpu->hl, &cpu->_hl);
}

/**
 * exchange contents of a register pair with memory pointed to by SP
 * \param reg register to operate on
 * \param cpu z80 cpu object
 * \param memory pointer to allocated memory
 */
void
_ex_sp_reg16(word *reg, struct z80 *cpu, uint8_t *memory)
{
	if (memory[cpu->sp.W + 1] != reg->B.h) {
		reg->B.h ^= memory[cpu->sp.W + 1];
		memory[cpu->sp.W + 1] ^= reg->B.h;
		reg->B.h ^= memory[cpu->sp.W + 1];
	}

	if (memory[cpu->sp.W] != reg->B.l) {
		reg->B.l ^= memory[cpu->sp.W];
		memory[cpu->sp.W] ^= reg->B.l;
		reg->B.l ^= memory[cpu->sp.W];
	}
}
