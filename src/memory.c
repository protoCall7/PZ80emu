/** 
 * \file memory.c
 * \author Peter H. Ezetta
 * \date 2015-05-03
 */

#include "../config.h"

#ifdef HAVE_ERR
# include <err.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include "memory.h"

/**
 * Loads the contents of a ROM file into memory
 * \param filename String containing the filename of the ROM image.
 * \param self Pointer to the block of RAM to load the ROM into.
 * \return Number of bytes loaded into RAM.
 */
static long
memory_load(void *self, const char *filename)
{
	struct memory *mem = self;
	FILE *infile;
	long file_numbytes;
	size_t readbytes;

	infile = fopen(filename, "r");
	if (infile == NULL) {
		warn(NULL);
		return -1;
	}

	if((fseek(infile, 0L, SEEK_END)) != 0) {
		warn(NULL);
		fclose(infile);
		return -1;
	}

	file_numbytes = ftell(infile);

	if((fseek(infile, 0L, SEEK_SET)) != 0) {
		warn(NULL);
		fclose(infile);
		return -1;
	}

	readbytes = fread(mem->memory, sizeof(char), (size_t)file_numbytes, infile);
	if(readbytes != (size_t)file_numbytes) {
		warn(NULL);
		fclose(infile);
		return -1;
	}

	if((fclose(infile)) != 0) {
		warn(NULL);
		return -1;
	}

	return file_numbytes;
}

/**
 * Frees an allocated memory object
 * \param self memory object to free
 */
static void
memory_free(void *self)
{
	struct memory *mem = self;

	free(mem->memory);
	free(mem);
}

/**
 * Allocates a block of RAM of size MEMSIZE
 * \return Pointer to allocated block of RAM.
 */
struct memory *
memory_new(void)
{
	/* allocate memory */
	struct memory *mem;

	mem = malloc(sizeof(struct memory));
	if (mem == NULL)
		err(1, NULL);

	if ((mem->memory = calloc(MEMSIZE, 1)) == NULL)
		err(1, NULL);

	/* tie methods to the object */
	mem->memory_load = &memory_load;
	mem->memory_free = &memory_free;

	return mem;
}
