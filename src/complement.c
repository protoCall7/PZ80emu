/**
 * \file complement.c
 * \author Peter H. Ezetta
 */

#include "complement.h"
#include "utils.h"

/**
 * sets the carry flag
 * \param cpu z80 cpu object
 */
void
_scf(struct z80 *cpu)
{
	SET(cpu->af.B.l, CARRY);
	RESET(cpu->af.B.l, SUBTRACT);
	RESET(cpu->af.B.l, HALF_CARRY);
}

/**
 * clears the carry flag
 * \param cpu z80 cpu object
 */
void
_ccf(struct z80 *cpu)
{
	TOGGLE(cpu->af.B.l, CARRY);
	RESET(cpu->af.B.l, SUBTRACT);
}
