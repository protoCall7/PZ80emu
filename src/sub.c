/**
 * \file sub.c
 * \brief Helper functions for subtraction operations
 * \author Peter H. Ezetta
 * \date 2018-2019
 */

#include "sub.h"
#include "utils.h"

/**
 * \brief sets the carry flag based on the provided operands
 * \param a
 * \param b
 * \param cpu z80 CPU object
 */
static void
___sub8_set_carry_flag(uint8_t a, uint8_t b, struct z80 *cpu)
{
	if ((a - b) < 0)
		SET(cpu->af.B.l, CARRY);
	else
		RESET(cpu->af.B.l, CARRY);
}

/**
 * \brief sets the zero flag based on the provided operands
 * \param a
 * \param b
 * \param cpu z80 CPU object
 */
static void
___sub8_set_zero_flag(uint8_t a, uint8_t b, struct z80 *cpu)
{
	if ((a - b) == 0)
		SET(cpu->af.B.l, ZERO);
	else
		RESET(cpu->af.B.l, ZERO);
}

/**
 * \brief sets the overflow flag based on the provided operands
 * \param a
 * \param b
 * \param cpu z80 CPU object
 */
static void
___sub8_set_overflow_flag(uint8_t a, uint8_t b, struct z80 *cpu)
{
	if (a > 0) {
		if ((a - b) < 0)
			SET(cpu->af.B.l, PARITY_OVERFLOW);
		else
			RESET(cpu->af.B.l, PARITY_OVERFLOW);
	} else
		RESET(cpu->af.B.l, PARITY_OVERFLOW);
}

/**
 * \brief sets the sign flag based on the provided operands
 * \param a
 * \param b
 * \param cpu z80 CPU object
 */
static void
___sub8_set_sign_flag(uint8_t a, uint8_t b, struct z80 *cpu)
{
	if ((127 < (a - b)) && ((a - b) < 256))
		SET(cpu->af.B.l, SIGN);
	else
		RESET(cpu->af.B.l, SIGN);
}

/**
 * \brief sets the half carry flag based on the provided operands
 * \param a
 * \param b
 * \param cpu z80 CPU object
 */
static void
___sub8_set_half_carry_flag(uint8_t a, uint8_t b, struct z80 *cpu)
{
	if (((a & 0x0F) - ((b & 0x0F) & 0x10) == 0x10))
		SET(cpu->af.B.l, HALF_CARRY);
	else
		RESET(cpu->af.B.l, HALF_CARRY);
}

/**
 * \brief wrapper to set flags based on the operands of a subtraction operation
 * \param a
 * \param b
 * \param cpu z80 CPU object
 */
static void
__sub8_set_flags(uint8_t a, uint8_t b, struct z80 *cpu)
{
	___sub8_set_carry_flag(a, b, cpu);
	___sub8_set_zero_flag(a, b, cpu);
	___sub8_set_overflow_flag(a, b, cpu);
	___sub8_set_sign_flag(a, b, cpu);
	___sub8_set_half_carry_flag(a, b, cpu);

	SET(cpu->af.B.l, SUBTRACT);
}

/**
 * \brief set flags as if operand was subtracted from A
 * \param reg register to subtract from A
 * \param cpu z80 CPU object
 *
 * The CP instruction sets the flags as if the operand to CP were subtracted
 * from the A register, however the subtraction does not actually occur, and
 * the original value of A is left untouched.
 */
void
_cp(uint8_t *reg, struct z80 *cpu)
{
	__sub8_set_flags(cpu->af.B.h, *reg, cpu);
}

/**
 * \brief subtracts reg from the A register
 * \param cpu z80 CPU object
 * \param reg 8-bit register to subtract from A
 */
void
_sub_a_reg8(struct z80 *cpu, uint8_t *reg)
{
	__sub8_set_flags(cpu->af.B.h, *reg, cpu);
	cpu->af.B.h -= *reg;
}

/**
 * \brief subtracts the contents of a memory address from the A register
 * \param cpu z80 CPU object
 * \param memory pointer to memory to subtract from the A register
 */
void
_sub_a_mem(struct z80 *cpu, uint8_t *memory)
{
	uint8_t value = memory[cpu->pc.W++];

	__sub8_set_flags(cpu->af.B.h, value, cpu);
	cpu->af.B.h -= value;
}

/**
 * \brief subtracts an 8-bit register from the A register with carry
 * \param cpu z80 CPU object
 * \param reg register to subtract from A
 */
void
_sbc_a_reg8(struct z80 *cpu, uint8_t *reg)
{
	if (IS_SET(cpu->af.B.l, CARRY))
		cpu->af.B.h--;

	__sub8_set_flags(cpu->af.B.h, *reg, cpu);
	cpu->af.B.h -= *reg;
}

/**
 * \brief decrements an 8-bit register
 * \param n register to decrement
 * \param cpu z80 CPU object
 * \bug dec8 isn't operating on flags
 */
void
_dec8(uint8_t *n, struct z80 *cpu)
{
	__sub8_set_flags(*n, 1, cpu);
	*n = *n - 1;
}
