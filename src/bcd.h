/**
 * \file bcd.h
 * \author Peter H. Ezetta
 */

#ifndef __PZ80EMU_BCD__
#define __PZ80EMU_BCD__

#include "z80.h"

__BEGIN_DECLS
void _daa(struct z80 *);
__END_DECLS

#endif
