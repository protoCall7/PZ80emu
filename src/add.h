/**
 * \file add.h
 */

#ifndef __PZ80EMU_ADD__
#define __PZ80EMU_ADD__

#include <stdint.h>

#include "z80.h"

__BEGIN_DECLS
void _inc8(uint8_t *, struct z80 *);
void _add_reg16_reg16(word *, word *, struct z80 *);
void _add_a_mem(struct z80 *, uint8_t *);
void _add_a_reg8(struct z80 *, uint8_t *);
void _adc_a_reg8(struct z80 *, uint8_t *);
void _adc_a_mem(struct z80 *, uint8_t *);
__END_DECLS

#endif
