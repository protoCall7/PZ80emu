.POSIX:

include Makefile.configure

LDLIBS       = `pkg-config --libs ncurses`
OBJS         = add.o \
	       bit.o \
               boolean.o \
               bcd.o \
               compats.o \
               complement.o \
               display.o \
               exchange.o \
               interpreter.o \
               interrupt.o \
               io.o \
               jump.o \
               load.o \
               memory.o \
               pz80emu.o \
               rotate.o \
               stack.o \
               sub.o \
               z80.o
PROG         = pz80emu
TEST_CFLAGS  = $(CFLAGS) -I/usr/local/include
TEST_LDFLAGS = -L/usr/local/lib
TEST_LDLIBS  = -lcheck
TESTS        = check
VPATH        = src/

all: $(PROG)
$(PROG): $(OBJS)
	$(CC) $(CFLAGS) -o $@ $(OBJS) $(LDFLAGS) $(LDLIBS)
add.o: src/add.c src/add.h src/z80.h src/bus.h src/utils.h \
  src/memory.h
bcd.o: src/bcd.c src/bcd.h src/z80.h src/bus.h src/utils.h
boolean.o: src/boolean.c src/boolean.h src/z80.h src/bus.h src/utils.h
bit.o: src/bit.c src/bit.h src/z80.h src/utils.h
compats.o: compats.c
complement.o: src/complement.c src/complement.h src/z80.h src/bus.h \
  src/utils.h
display.o: src/display.c src/z80.h src/bus.h src/display.h src/utils.h
exchange.o: src/exchange.c src/exchange.h src/z80.h src/bus.h
interpreter.o: src/interpreter.c src/interpreter.h src/z80.h \
  src/bus.h src/add.h src/bcd.h src/boolean.h src/complement.h \
  src/exchange.h src/interrupt.h src/io.h src/jump.h src/load.h \
  src/rotate.h src/stack.h src/sub.h src/utils.h
interrupt.o: src/interrupt.c src/interrupt.h src/z80.h src/bus.h
io.o: src/io.c src/io.h src/z80.h src/bus.h
jump.o: src/jump.c src/jump.h src/z80.h src/bus.h src/utils.h
load.o: src/load.c src/load.h src/z80.h src/bus.h src/memory.h
memory.o: src/memory.c src/memory.h
pz80emu.o: src/pz80emu.c src/z80.h src/bus.h src/memory.h src/display.h
rotate.o: src/rotate.c src/rotate.h src/z80.h src/bus.h src/utils.h
stack.o: src/stack.c src/stack.h src/z80.h src/bus.h src/utils.h
sub.o: src/sub.c src/sub.h src/z80.h src/bus.h src/utils.h
z80.o: src/z80.c src/z80.h src/bus.h src/display.h src/interpreter.h
check_add.o: tests/check_add.c tests/check_add.h add.o src/add.c src/add.h
	$(CC) $(TEST_CFLAGS) $(TEST_LDFLAGS) -c tests/check_add.c
check: tests/check_main.c tests/check_add.h check_add.o
	$(CC) $(TEST_CFLAGS) -o $@ tests/check_main.c check_add.o add.o $(TEST_LDFLAGS) $(TEST_LDLIBS)
$(OBJS) $(TESTS): config.h
add.o bcd.o boolean.o jump.o rotate.o stack.o sub.o: src/z80.h src/bus.h src/utils.h 
docs:
	doxygen
cppcheck:
	cppcheck src/*
tags:
	ctags src/*.c
clean:
	rm -f *.o *.core $(PROG) $(TESTS) tags
	rm -rf *.dSYM/
distclean: clean
	rm -f Makefile.configure config.*
	rm -rf doc/
test: check
	./check
